+++
tags = ["devops", "infrastructure", "container", "linux", "kernel" ]
description = "Tetragon File Monitoring"
title = "Tetragon Filemon"
+++
## File access traces with Tetragon

Tracing policies can be added to Tetragon through YAML configuration files that extend Tetragon’s base execution tracing capabilities. These policies perform filtering in kernel to ensure only interesting events are published to userspace from the BPF programs running in kernel. This ensures overhead remains low even on busy systems.

https://tetragon.io/docs/getting-started/file-events/ 
## Tetragon 1.0: Kubernetes Security Observability & Runtime Enforcement with eBPF

https://isovalent.com/blog/post/tetragon-release-10/#observability-benchmarks-understanding-tetragon-performance
## File Monitoring with eBPF and Tetragon (Part 1)

https://isovalent.com/blog/post/file-monitoring-with-ebpf-and-tetragon-part-1/