+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Kubernetes ReplicaSets & Deployments description"
title = "Replicasets & Deployments"
+++
### Links
- [K8s Cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
- [K8s Conventions](https://kubernetes.io/docs/reference/kubectl/conventions/)
## ReplicaSets
ReplicaSet следит за тем, чтобы количество Pod всегда было заданным числом штук (параметр replicas) - не больше и не меньше.
ReplicaSet Controller является более современным аналогом ReplicationController:
- ReplicationController apiVersion = v1
- ReplicaSetController apiVersion = apps/v1

Отличие ReplicaSet в том, что в нём обязательным параметром есть selectors, который отслеживает контейнеры, в том числе созданные ДО создания самого ReplicaSet. Отслеживание можно производить по их меткам labels. Для совпадающих меток работает алгоритм приведения к нужному количеству. 

Создание ReplicaSet:
```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: myreplicaset
  labels:
    name: myapp
spec:
  selector:
    matchLabels:
      env: production ### метка должна совпадать
  replicas: 3
  template:
    metadata:  ### шаблон контейнера берётся из описания Pod
      name: myapp
      labels:
        env: production ### метка должна совпадать
    spec:
      containers:
        - name: nginx-container
          image: nginx
    
```

Шаблон Pod всегда должен быть описан в части template, чтобы ReplicaSet знал, какие Pod создавать. 
### Команды для работы с ReplicaSets
```shell
kubectl create -f <имя файла с описанием replicaset>
kubectl get rs # вывести все ReplicaSet в кластере
kubectl describe rs <имя replicaset> # подробности о ReplicaSet
kubectl delete rs <имя replicaset> # удаляет все Pods и сам ReplicaSet
kubectl edit rs <имя replicaset> # отредактировать описание Replicaset
kubectl scale rs <имя replicaset> --replicas=<новое количество копий> 
kubectl replace -f <имя файла с описанием replicaset> # заменить ReplicaSet 
```

## Deployments
Deployment - это надстройка над ReplicaSet, добавляет логику обновления Pod с одной версии на другую. При обновлении Deployment имеет 2 стратегии:
- Rolling Update - (по умолчанию) при обновлении Pods, делает это поштучно: один Pod старой версии кладёт (Terminated), сразу поднимает заместо него новый Pod;
- Recreate - при обновлении сначала удаляются все Pods, после чего взамен поднимаются новые. Сопровождается падением/отключением приложения для потребителей.
```shell
kubectl apply -f <имя Deployment> # запустить процесс rollout после внесения изменений в манифест
kubectl rollout status <имя Deployment> # узнать о статусе выкатывания

kubectl rollout history <имя Deployment> # узнать о всех ревизиях и причинах их перехода
kubectl rollout history <deployment> --revision=1 # узнать статус конкретной версии

kubectl rollout undo <имя Deployment> # откатить назад обновление Deployment
kubectl rollout undo <имя Deployment> --to-revision=1 # откатить до версии 1
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mydeployment
  labels:
    name: myapp
spec:
  selector:
    matchLabels:
      env: production ### метка должна совпадать
  replicas: 3
  strategy: 
    type: RollingUpdate ### стратегия замены Pods
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:  ### шаблон контейнера берётся из описания Pod
      name: myapp
      labels:
        env: production ### метка должна совпадать
    spec:
      containers:
        - name: nginx-container
          image: nginx
    
```
### Команды для работы с Deployments
```shell
kubectl create -f <имя файла с описанием Deployment>
kubectl get deploy # вывести все Deployment в кластере
kubectl describe deploy <имя Deployment> # подробности о Deployment
kubectl delete rs <имя Deployment> # удаляет все Pods и сам Deployment
kubectl edit rs <имя Deployment> # отредактировать описание Deployment и произвести его обновление
kubectl edit rs <имя Deployment> --record # отредактировать описание, вызвав обновление и записать команду как причину обновления в список ревизий
kubectl set image deploy <имя Deployment> nginx=nginx:1.18 # пример обновления без редактирования YAML
```
### Императивные команды
В отличие от декларативных, такие команды позволяют быстро решить однократную задачу.
```shell
kubectl create deploy nginx --image=nginx --replicas=4
kubectl scale deploy nginx --replicas=4

kubectl create deployment nginx --image=nginx --dry-run=client -o yaml > nginx-deployment.yaml # вывести манифест YAML для последующего редактирования 
```