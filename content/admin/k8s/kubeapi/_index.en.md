+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Kube-apiserver Description"
title = "Kube-apiserver"
+++

## Описание kube-apiserver
* При работе с кластером k8s через инстумент kubectl, по сути работа идёт именно с kube-apiserver;
* Kube-apiserver забирает данные из БД Etcd и возвращает их пользователю kubectl.

Работа kube-apiserver по созданию pod:
1. Аутентификация пользователя; 
2. Валидация запроса о создании pod;
3. Получение данных;
4. Обновление Etcd;
5. kube-scheduler мониторит kube-apiserver, узнаёт о появлении pod без привязки к worker-node. Он находит node, куда положить pod и сообщает в kube-apiserver;
6. kube-apiserver обновляет данные в Etcd;
7. kube-apiserver передаёт данные на kubelet выбранного worker-node;
8. Kubelet создаёт на своей worker-node нужный pod и раскатывает там образ контейнера через CRE.

### Настройки
* Если k8s ставился через kubeadm, то они находятся в 
  `/etc/kubernetes/manifests/kube-apiserver.yaml`
* Если k8s ставился вручную, то они находятся в 
  `/etc/systemd/system/kube-apiserver.service`

