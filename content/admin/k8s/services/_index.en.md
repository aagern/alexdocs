+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s", "network" ]
description = "Kubernetes Services"
title = "Services"
+++
### Links
- [K8s Cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
- [K8s Conventions](https://kubernetes.io/docs/reference/kubectl/conventions/)
## Services
Service - логический объект в k8s, который позволяет внешним клиентам подключаться к сетевым портам на контейнерах внутри кластеров Pod. 

![](svc_ports.jpeg)

Service делятся на 3 вида:
- NodePort - публикация порта с Pod наружу на уровень Worker Node (причём, если Pods размазаны лишь по части от всех нод кластера k8s, то всё равно они доступны при обращении К ЛЮБОЙ ноде кластера);
- ClusterIP - по сути внутренний балансировщик для обращения, например, части frontend приложения к множеству Pods, реализующих backend;
- LoadBalancer - NodePort с заданием стратегии балансировки (в NodePort - случайный алгоритм балансировки), поддерживается лишь на ряде облачных площадок (AWS, GCM и т.д.). В остальных площадках ведёт себя как NodePort.
### NodePort
```yaml
apiVersion: v1
kind: Service
metadata:
  name: myservice
spec: 
  type: NodePort
  ports:
    - port: 80
      targetPort: 80
      nodePort: 30004 # порты ограничены диапазоном 30000-32767
  selector:
    app: myapp
```
### ClusterIP
```yaml
apiVersion: v1
kind: Service
metadata:
  name: myservice
spec: 
  type: ClusterIP
  ports:
    - port: 80
      targetPort: 80
  selector:
    app: myapp
```

### LoadBalancer
```yaml
apiVersion: v1
kind: Service
metadata:
  name: myservice
spec: 
  type: LoadBalancer # only works with a supported platform
  ports:
    - port: 80
      targetPort: 80
  selector:
    app: myapp
```

## Команды работы с Service
```shell
kubectl create -f <имя файла с описанием Service>
kubectl get svc # вывести все Service в кластере
kubectl describe svc <имя service> # подробности о service
kubectl delete svc <имя service> # удаляет все объект service
kubectl edit svc <имя service> # отредактировать описание service и произвести его обновление
```

### Императивные команды
В отличие от декларативных, такие команды позволяют быстро решить однократную задачу.
```shell
kubectl create service clusterip redis --tcp=6379:6379 --dry-run=client -o yaml # нельзя подавать selectors в команду, следует вывести YAML и отредактировать, что небыстро

kubectl expose pod redis --port=6379 --name=redis-service --dry-run=client -o yaml # такой вариант CLusterIP использует labels самого pod как selectors, что намного эффективнее

kubectl expose pod nginx --port=80 --name=nginx-service --type=NodePort --dry-run=client -o yaml # такой вариант NodePort использует labels самого pod как selectors
```

