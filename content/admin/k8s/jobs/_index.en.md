+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Kubernetes Jobs & CronJobs"
title = "Jobs & CronJobs"
+++
## Jobs
Это объект для выполнения однократных служебных задач.
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: my-job
spec:
  completions: 3 # сколько Pod-ов запускать под задачу
  parallelism: 3 # запускать Pod-ы не последовательно, а сразу пачками по 3
  # если 1 из 3 Pod завершится с ошибкой, k8s будет 1 оставшийся перезапускать,    # пока тот не закончит работу корректно
  template:
    spec:
      containers:
      - name: job-container
        image: busybox
        command: [ "/run/job" ]
      restartPolicy: Never
```

Команды для работы с Jobs:
```shell
kubectl create -f <имя job.yaml>
kubectl get jobs
kubectl logs <имя Pod с Job> # вывод результата
kubectl delete job <имя Pod>
```
## CronJobs
Объект для создания периодической задачи:
```yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: my-cronjob
spec:
  schedule: "*/1 * * * *" # работает как Cron в Linux, см ниже Cron Parameters
  jobTemplate: # ниже описание spec обычного Job
    spec:
      completions: 3
      parallelism: 3 
      template:
        spec:
          containers:
          - name: job-container
            image: busybox
            command: [ "/run/job" ]
          restartPolicy: Never
```

Cron parameters:
```
# ┌───────────── minute (0 - 59)
# │ ┌───────────── hour (0 - 23)
# │ │ ┌───────────── day of the month (1 - 31)
# │ │ │ ┌───────────── month (1 - 12)
# │ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday;
# │ │ │ │ │                                   7 is also Sunday on some systems)
# │ │ │ │ │
# │ │ │ │ │
# * * * * * <command to execute>
```
