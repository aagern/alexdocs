+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Node Selectors and Affinity"
title = "Selectors and Affinity"
+++

## Node Selectors
Добавить пометки к node можно командой:  
```shell
kubectl label nodes <node-name> <label-key>=<label-value>

kubectl label nodes node-01 size=Large # пример
```

После этого в описании Pod можно указать Node Selector:
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: myapp
spec:
  containers:
  - name: data-processor
    image:: data-processor
  nodeSelector:
    size: Large
```

Node Selectors работают по принципу 1:1 совпадения метки Node и Pod. Для более сложных сценариев применяют Node Affinity.

## Node Affinity
Для создание свойств Node Affinity нужно поменять свойства в манифесте Pod:
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: myapp
spec:
  containers:
  - name: data-processor
    image:: data-processor
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: size
            operator: In # условие, может быть наоборот NotIn, или Exists - есть ли вообще такой label, необязательно имеющий значение?
            values:
            - Large # условие действует на любое из значений списка
            - Medium
```

В случае, если нужные labels отсутствуют на Nodes кластера, есть 2 типа поведения, которое задаётся свойством Pod:

- `requiredDuringSchedulingIgnoredDuringExecution` - если Nodes с нужными labels нет, вообще не размещать данный Pod на кластере;
- `preferredDuringSchedulingIgnoredDuringExecution` - если Nodes с нужными labels нет, всё равно разместить данный Pod где-нибудь на кластере.

Если Pod уже запущен на Node в момент, когда добавили label, то в версии 1.27 ничего не произойдёт в обоих случаях. В плане добавить третий тип поведения: 
- `requiredDuringSchedulingRequiredDuringExecution` - если во время работы Pod произойдёт изменение affinity - удалить Pod с Node.
