+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Readiness & Liveness Probes configuring and logging"
title = "Readiness Probes"
+++
## Readiness Probes
Определение, что ПО в контейнер действительно запустилось успешно и готово принимать данные пользователей, можно провести по-разному, добавив в манифест раздел `spec -> containers` поле `readinessProbe`.
- Для проверки HTTP сервера:
   ```yaml 
   readinessProbe:
     httpGet:
       path: /api/ready
       port: 8080
     initialDelaySeconds: 10 # предусматриваем 10 сек задержку на старте
     periodSeconds: 5 # повторяем проверку спустя 5 секунд
     failureThreshold: 8 # повторяем проверку 8 раз (по умолчанию 3)
  ```

- Для проверки открытого порта (например, у СУБД):
  ```yaml
  readinessProbe:
    tcpSocket:
      port: 3306
  ```

- Для проверки с помощью своей команды:
  ```yaml
  readinessProbe:
    exec:
      command: 
        - cat
        - /app/is_ready
  ```
## Liveness Probes
Периодическое определение, работает ли ПО в контейнере - для случаев, когда падение ПО не приводит к его вылету и закрытию контейнера.
- Для проверки HTTP сервера:
   ```yaml 
   livenessProbe:
     httpGet:
       path: /api/health_status
       port: 8080
     initialDelaySeconds: 10 # предусматриваем 10 сек задержку на старте
     periodSeconds: 5 # повторяем проверку спустя 5 секунд
     failureThreshold: 8 # повторяем проверку 8 раз (по умолчанию 3)
  ```

- Для проверки открытого порта (например, у СУБД):
  ```yaml
  livenessProbe:
    tcpSocket:
      port: 3306
  ```

- Для проверки с помощью своей команды:
  ```yaml
  livenessProbe:
    exec:
      command: [ "cat", "/app/is_working" ]
  ```

## Container Logging
Для получения логов с Pod:
```bash
kubectl logs {-f} <pod name> <container name> # -f = tail

kubectl logs -f myapp logger # пример, в случае нескольких контейнеров в Pod выбран контейнер logger 
```
