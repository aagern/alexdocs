+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Pod taints and tolerations"
title = "Taints & Tolerations"
+++

Для распределения Pods по Nodes применяется сочетание покраски (taint) и восприимчивости (toleration) к ней. 
## Taints
Покраска Node говорит kube-scheduler, что есть 1 из 3 эффектов:
- NoSchedule - не назначать сюда Pods без toleration;
- PreferNo Schedule - назначать Pods без toleration с наименьшим приоритетом, если больше некуда;
- NoExecute - не назначать сюда Pods без toleration, уже имеющиеся тут Pods удалить и перенести куда-то ещё. 

Покраска node:
```shell
kubectl taint nodes <имя node> key=value:effect

kubectl taint nodes node01 app=myapp:NoSchedule # пример
kubectl taint nodes node01 app=myapp:NoSchedule- # минус в конце снимает покрас
```

## Tolerations
Поменять восприимчивость Pod к покраске:
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
spec:
  containers:
  - name: nginx-controller
    image: nginx
  tolerations:
  - key: "app"
    operator: "Equal"
    value: "blue"
    effect: "NoSchedule"
```
