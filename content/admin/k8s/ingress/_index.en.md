+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s", "network" ]
description = "Kubernetes Ingress Controller"
title = "Ingress"
+++

## Links
- [Параметры nginx ingress](https://kubernetes.github.io/ingress-nginx/examples/)
## Ingress Controller
Объект служит в роли прокси и балансировщика L7.

Создание  минимального ingress:
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata: 
  name: minimal-ingress-controller
spec:
  replicas: 1
  selector:
    matchLabels:
      name: nginx-ingress
  template:
    metadata:
      labels:
        name: nginx-ingress
    spec: 
      containers:
        - name: nginx-ingress-controller
          image: quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.21.0
          args: 
            - /nginx-ingress-controller
            - --configmap=$ (POD_NAMESPACE)/nginx_configuraruin
          env:            # nginx требует 2 переменные для конфигурации
            - name: POD_NAME
              valueFrom: 
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom: 
                fieldRef:
                  fieldPath: metadata.namespace
          ports:
            - name: http
              containerPort: 80
            - name: https
              containerPort: 443
```

Для конфигурации ingress на nginx также нужен ConfigMap. В него закладывается конфигурация nginx, которая в обычном варианте nginx как reverse-proxy вписывалась в config самого nginx:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-configuration
```

Также необходимо создать Service, который публикует ingress вовне:
```yaml
apiVersion: v1
kind: Service
metadata: 
  name: nginx-ingress
spec: 
  type: NodePort
  ports:
  - port: 80
    targetPort: 80
    protocol: TCP
    name: http
  - port: 443
    targetPort: 443
    protocol: TCP
    name: https
  selector:
    name: nginx-ingress
```

И нужен Service Account для аутентификации:
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: nginx-ingress-serviceaccounts
```

### Ingress Resource
Набор правил ingress называются Ingress Resource.
```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ingress-rules
spec:
  backend:
    serviceName: app-service
    servicePort: 80
```

Определение нахождения правил ingress resource:
```shell
kubectl get ingress -A # найти Ingress Resource среди Namespaces 
```

Редактирование правил ingress resource:
```shell
kubectl edit ingress <имя ingress resource> -n <namespace>
```

### Nginx rewrite rules
Настройка `rewrite-target` нужна, чтобы правильно транслировать сетевой путь. 
```yaml
# Без rewrite-target:
http://<ingress-service>:<ingress-port>/watch --> http://<watch-service>:<port>/path

# С rewrite-target типа replace("/path","/"):
http://<ingress-service>:<ingress-port>/watch --> http://<watch-service>:<port>/
```

Для включения правил rewrite, нужно добавить в манифест annotations:
```yaml
1. apiVersion: extensions/v1beta1
2. kind: Ingress
3. metadata:
4.   name: test-ingress
5.   namespace: critical-space
6.   annotations:               # применение rewrite правил
7.     nginx.ingress.kubernetes.io/rewrite-target: /
8. spec:
9.   rules:
10.   - http:
11.       paths:
12.       - path: /pay
13.         backend:
14.           serviceName: pay-service
15.           servicePort: 8282
```