+++
tags = ["devops", "infrastructure", "container", "cluster", "database", "kubernetes", "k8s" ]
description = "ETCD in k8s"
title = "ETCD"
+++

## Особенности
 * Служба Etcd слушает на TCP2379;
 * Клиент - etcdctl;
 *  По умолчанию, etcdctl использует API v2. Переключать на API v3 нужно явно. Команды в API v2 и v3 отличаются:
```shell
# ETCD API v2.x
./etcdctl set key1 value1
./etcdctl get key1
./etcdctl --version # важно увидеть версию API (2), от этого команды зависят

# ETCD API v3.x
./etcdctl version # в API 3.x параметр version => команда, набирать без "--"
./etcdctl put key1 value1
./etcdctl get key1
./etcdctl get / --prefix -keys-only # вывести ключи в БД, без значений 
export ETCDCTL_API=3 # поменять версию API на 3.х
```

Для аутентификации клиента ETCDCTL на ETCD API Server нужно указывать также сертификат:
```shell
--cacert /etc/kubernetes/pki/etcd/ca.crt     
--cert /etc/kubernetes/pki/etcd/server.crt     
--key /etc/kubernetes/pki/etcd/server.key
```

Отсюда полноценная команда-запрос клиента к ETCD будет выглядет вот так:
```shell
kubectl exec etcd-master -n kube-system -- sh -c "ETCDCTL_API=3 etcdctl get / --prefix --keys-only --limit=10 --cacert /etc/kubernetes/pki/etcd/ca.crt --cert /etc/kubernetes/pki/etcd/server.crt  --key /etc/kubernetes/pki/etcd/server.key"
```