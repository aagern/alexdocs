+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Components of k8s Controller Manager"
title = "Controller Manager"
+++

## Node Controller
Отслеживает состояние нод (через kube-apiserver):
* Отслеживает статус; 
* Реагирует на проблемы;
* Node Monitor Period = 5 секунд на опрос всех нод через kube-apiserver;
* Node Monitor Grace Period = 40 секунд время ожидания, если нода не отвечает - отметить её как Unreachable;
* POD Eviction Timeout = 5 минут на ожидание возврата ноды, иначе перенос pod с этой ноды на другие ноды. 

## Replication Controller
Отслеживает множества replica sets, и что нужное число pods всегда доступны в каждом replica set.

## Другие подсистемы
* Deployment Controller
* Namespace Controller
* Job Controller
* PV-Protection Controller
* Endpoint Controller
* CronJob
* Service Account COntroller
* Stateful-Set
* Replica set
* и т.д.