+++
tags = ["devops", "infrastructure", "container", "cluster", "kubernetes", "k8s" ]
description = "Kubernetes Administration"
title = "Kubernetes"
+++
## Статьи в разделе

{{% children sort="weight" description="true" %}}


[![](https://mermaid.ink/img/pako:eNqNks1qwzAQhF9F6BRDHBIdTSkUu4dS0pa20IsOVuUlNrYls1qVhiTvXtdxDw3-iU5iZjT7CfbAtc2ARzwMQ2mooAoi9ug_AQ0QOHaHOi8INHkEabqQrpRzSaF2qGppWHs6haVb5QiQPbWF6dn4J7GI3b_HybBTtiNDp3PIfAU4nImtIbQV2yqjdmOhrkg1hQP8AlwE0gzlbo6r1fGWpR8Wy15k6016dVSkf73934eLLtQerwKa9MMG7fd-NBK_Pox6L8_JYr0Jpn0RTMGLQXgxAy_m4cUEvJiBFxfw0vAlrwFrVWTt8h5-30hOOdQgedReM4Wl5NKc2pzyZN_2RvOI0MOS-yZTBP0Gn8XTD-Xn9Vg?type=png)](https://mermaid.live/edit#pako:eNqNks1qwzAQhF9F6BRDHBIdTSkUu4dS0pa20IsOVuUlNrYls1qVhiTvXtdxDw3-iU5iZjT7CfbAtc2ARzwMQ2mooAoi9ug_AQ0QOHaHOi8INHkEabqQrpRzSaF2qGppWHs6haVb5QiQPbWF6dn4J7GI3b_HybBTtiNDp3PIfAU4nImtIbQV2yqjdmOhrkg1hQP8AlwE0gzlbo6r1fGWpR8Wy15k6016dVSkf73934eLLtQerwKa9MMG7fd-NBK_Pox6L8_JYr0Jpn0RTMGLQXgxAy_m4cUEvJiBFxfw0vAlrwFrVWTt8h5-30hOOdQgedReM4Wl5NKc2pzyZN_2RvOI0MOS-yZTBP0Gn8XTD-Xn9Vg)

### Master Node
Мастер нода представляет службы управления (Control Plane)
* Кластер Etcd ведёт запись всех нод, размещённых на них контейнерах, запись иных данных - это СУБД для Kubernetes, заточенная на согласованности данных и их доступности;
* Kube-scheduler: команды создания и переноса контейнеров на worker nodes. Считает число ресурсов на нодах и подбирает размещение pods на нодах в соответствии с профилем потребляемых ресурсов;
* Kube API Server: служба обмена сообщениями в кластере k8s. Аутентификация отправителя сообщения, валидирует отправителя сообщений, регистрирует сообщения по интерфейсу API в базу Etcd; Это единственный компонент, который общается напрямую с Etcd; 
* Kube Controller Manager: содержит службы контроля Node Controller (следит за доступностью нод), Replication Controller (отслеживание распространения копий контейнеров в рамках группы репликации по нодам).

### Worker Nodes
Ноды-работники размещают у себя контейнеры через Container Runtime Interface (CRI) с поддержкой containerd (через него Docker, с версии k8s 1.24+) и Rocket:
* Для приёма команд и передачи статистики по рабочей ноде используется kubelet, служба управления нодой;

{{% notice style="note" %}}
Kubeadm не устанавливает автоматически Kubelet-ы. Они всегда ставятся вручную на worker nodes.
{{% /notice %}}

* Для связи с нодой применяется служба Kube-proxy. Создаёт правила проброса потоков данных от служб к pods, на которых они размещены. Один из способов - создание правил iptables;

#### crictl
Проверка и решение проблем с рабочими нодами. В отличие от утилит Docker, crictl понимает pods. 
```shell
crictl images # список образов
circtl ps -a # список контейнеров
crictl exec -i -t 288023742....aaabb392849 ls # запуск команды в контейнере
crictl logs 288023742....aaabb392849 # посмотреть лог контейнера
crictl pods 
```

## IDE
Для написания YAML-файлов хорошо подходит редактор с плагином, понимающим k8s. Пример: VSCode + Red Hat YAML plugin

В свойствах плагина найти пункт **Yaml: Schemas -> Edit in settings.json**
Добавить в конфиг:
```json
{
    "yaml.schemas": {
        
    "kubernetes": "*.yaml"
    },
    "redhat.telemetry.enabled": true
}
```
Это позволит все файлы YAML редактировать с учётом полей, принятых для k8s. 


