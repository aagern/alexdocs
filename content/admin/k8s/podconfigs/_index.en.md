+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Pod commands, arguments, env variables"
title = "Pod Commands & Configs"
+++
## Links
- [ConfigMap doc](https://kubernetes.io/docs/concepts/configuration/configmap/)
- [Secrets doc](https://kubernetes.io/docs/concepts/configuration/secret/)
## Commands & Arguments
Команды и аргументы команд, которые срабатывают при запуске контейнера.
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: myapp
  labels:
    app: test_app
    env: productinon
spec:
  containers:
    - name: nginx-container
      image: nginx
      command: [ "python3" ]
      args: [ "app-test.py" ]

## Вариант 2
      command: 
        - "python3"
        - "app-test.py"

## Вариант 3
      command: [ "python3", "app-test.py" ]
```

Заменять команды, аргументы, метки и т.д. нельзя. Однако, можно вызвать ошибку, потом пересоздать Pod на лету из сохранённого промежуточного файла:
```shell
$ kubectl edit pod nginx-container # отредактировал поле command

error: pods "nginx-container" is invalid
A copy of your changes has been stored to "/tmp/kubectl-edit-1395347318.yaml"
error: Edit cancelled, no valid changes were saved.

$ kubectl replace --force -f /tmp/kubectl-edit-1395347318.yaml
```
## Environmental Variables
Переменные среды задаются как список, похожим образом с командами.
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: myapp
  labels:
    app: test_app
    env: productinon
spec:
  containers:
    - name: nginx-container
      image: nginx
      env:
        - name: APP_COLOR 
          value: green
```
### ConfigMap
Отдельный объект, который содержит переменные среды. Можно получить их список  через `kubectl get configmaps`
```yaml
apiVersion: v1
kind: ConfigMap
metadata: 
  name: mydb
data:
  APP_COLOR: blue
  APP_MODE: testdev
```
#### Императивный способ создания ConfigMap
```shell
kubectl create configmap \ 
        <имя конфига> --from-literal=<ключ>=<значение> \
                      --from-literal=APP_USER=testuser

kubectl create configmap \
        <имя конфига> --from-file=<путь до файла>
                    # --from-file=app_config.properties
```
#### Ссылка на ConfigMap в описании Pod
Ссылка производится по именам ConfigMap в виде списка:
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: myapp
  labels:
    app: test_app
    env: productinon
spec:
  containers:
    - name: nginx-container
      image: nginx
      envFrom:
        - configMapRef: 
            name: mydb
            
# Вариант взять только конкретную переменную: 
      env: 
        - name: APP_COLOR 
          valueFrom: 
            configMapKeyRef:
              name: mydb
              key: APP_COLOR
```
## Secrets
Секреты - это ConfigMap, значения которых кодируются по base64. Можно получить их список  через:
```shell
kubectl get secrets # список секретов
kubectl describe secret <имя секрета> # не отображает значения
kubectl get secret <имя секрета> -o yaml # отображает значения в файле 
```
#### Декларативное описание
```yaml
apiVersion: v1
kind: Secret
metadata: 
  name: mydb
data:
  APP_PWD: dmVyeXNlY3JldA== # base64 Encode
  APP_TOKEN: dGVzdGRldg==
```
#### Императивный способ создания Secret
```shell
kubectl create secret generic \ 
        <имя конфига> --from-literal=<ключ>=<значение> \
                      --from-literal=APP_USER=testuser

kubectl create secret generic \
        <имя конфига> --from-file=<путь до файла>
                    # --from-file=app_config.properties
```
### Ссылка на Secret в описании Pod
```yaml
apiVersion: v1
kind: Pod
metadata: 
  name: myapp
  labels:
    app: test_app
    env: productinon
spec:
  containers:
    - name: nginx-container
      image: nginx
      envFrom:
        - secretRef: 
            name: mydb

# Вариант взять только конкретное значение: 
      env: 
        - name: APP_COLOR 
          valueFrom: 
            secretKeyRef:
              name: mydb
              key: APP_PWD
              
# Вариант смонтировать как файлы (каждый пароль - отдельный файл)
      volumes:
      - name: app-secret-volume
        secret: 
          secretName: app-secret
```
## Service Accounts
Специальные учётные записи для доступа к k8s. При создании вместе с ними создаётся объект secret. 
- С версии k8s 1.22 объект Secret имеет время жизни;
- С версии k8s 1.24 секрет не создаётся на автомате, нужно его отдельно создать:

```shell
kubectl create serviceaccount dashboard-sa
kubectl create token dashboard-sa # с k8s 1.24+ необходимо создать токен, у которого время жизни (по умолчанию) =1 час с момента создания
```
