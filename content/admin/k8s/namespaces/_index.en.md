+++
tags = ["devops", "infrastructure", "container", "cluster", "api", "kubernetes", "k8s" ]
description = "Namespaces in Kubernetes"
title = "Namespaces"
+++
## Create
Use manifest to create:
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: dev
```

Commands:
```bash
kubectl create -f namespace-dev.yaml
kubectl create namespace dev
```
### Pods in Namespace
To place pods in selected namespace add it to their manifest:
```yaml
apiVersion: v1
kind: Pod       
metadata: 
  namespace: dev
  name: myapp-pod
spec:
  containers:
    - name: nginx-container
      image: nginx
```

## Viewing Namespace Contents
Write the target namespace to get pods from:
```bash
kubectl get pods --namesapce=dev
```

Changing namespace in the current context of kubectl to **dev**:
```bash
kubectl config set-context $(kubectl config current-context) --namespace=dev 
```

List pods in all namespaces:
```bash
kubectl get pods --all-namespaces
```