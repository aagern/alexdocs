+++
tags = ["devops", "script", "bash", "syslog", "infrastructure", "log", "ubuntu", "parser" ]
description = "Grafana Loki installation, config & usage"
title = "Grafana Loki"
+++

## Installation
Create dirs for Loki:
```bash
cd ~
mkdir promtail
mkdir loki
mkdir grafana
```

### Create a Docker-Compose file
vim docker-compose.yml
```bash
version: "3"

networks:
loki:

services:
loki:
image: grafana/loki:2.4.0
volumes:
- /home/aagern/loki:/etc/loki
ports:
- "3101:3100"
restart: unless-stopped
command: -config.file=/etc/loki/loki-local-config.yaml
networks:
- loki

promtail:
image: grafana/promtail:2.4.0
volumes:
- /var/log:/var/log
- /home/aagern/promtail:/etc/promtail
restart: unless-stopped
command: -config.file=/etc/loki/promtail-local-config.yaml
networks:
- loki

grafana:
image: grafana/grafana:latest
user: "1000"
volumes:
- /home/aagern/grafana:/var/lib/grafana
ports:
- "3000:3000"
restart: unless-stopped
networks:
- loki
```

{{% notice style="note" %}}
The config files MUST be in the inner container directory!
{{% /notice %}}

### Loki Configs
```bash
wget https://raw.githubusercontent.com/grafana/loki/master/cmd/loki/loki-local-config.yaml
 
wget https://raw.githubusercontent.com/grafana/loki/main/clients/cmd/promtail/promtail-local-config.yaml
```

### Promtail config
```bash
server:
http_listen_port: 9080
grpc_listen_port: 0
 
positions:
filename: /tmp/positions.yaml
 
clients:
- url: http://loki:3101/loki/api/v1/push
 
# Local machine logs
scrape_configs:
- job_name: local
static_configs:
- targets:
- localhost
labels:
job: varlogs
__path__: /var/log/*log
 
#scrape_configs:
#- job_name: docker
# pipeline_stages:
# - docker: {}
# static_configs:
# - labels:
# job: docker
# path: /var/lib/docker/containers/*/*-json.log
```

### Create the containers
```bash
cd ~
sudo docker-compose up -d --force-recreate
sudo docker ps
```

### Data Sources

Go to Grafana <server ip>:3000
First login/password = **admin/admin**

**Add New Data Source... → Loki**  
[http://loki:3100](http://loki:3100)

Explore
{job=varlogs} |= "Network"

### Docker logs
```yaml
scrape_configs:
- job_name: docker
 pipeline_stages:
  - docker: {}
  static_configs:
   - labels:
   job: docker
   path: /var/lib/docker/containers/*/*-json.log
```
Install a Docker Driver [https://grafana.com/docs/loki/latest/clients/docker-driver/](https://grafana.com/docs/loki/latest/clients/docker-driver/)

```bash
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
sudo docker plugin ls
```
Check config: [https://grafana.com/docs/loki/latest/clients/docker-driver/configuration/](https://grafana.com/docs/loki/latest/clients/docker-driver/configuration/)

```bash
{
"debug" : true,
"log-driver": "loki",
"log-opts": {
"loki-url": "http://localhost:3100/loki/api/v1/push",
"loki-batch-size": "400"
}
}
```
- Config: ```vim /etc/docker/daemon.json```
- Restart Docker: ```sudo systemctl restart docker```




