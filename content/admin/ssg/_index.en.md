+++
tags = ["devops", "script", "bash", "control", "config", "code", "infrastructure", "golang" ]
description = "Static-Site-Generation"
title = "SSG Hugo"
+++
## Install (on NIX)
Need to install:
* Gitlab.com account
* Git client
* Hugo
	* Hugo theme ([Beautiful Hugo](https://themes.gohugo.io/themes/beautifulhugo/), [Relearn](https://themes.gohugo.io/themes/hugo-theme-relearn/))
* Markdown editor (Obsidian)

Choose a site directory and create site + repo:
```bash
hugo new site ./
git init
git add *
git commit -m "Initial commit"
cd themes/
git submodule add https://github.com/halogenica/beautifulhugo.git themes/beautifulhugo
git status
git add *
git commit -m "Theme added"
```

Some themes (Relearn) have to be downloaded as zip. So we have a git repo inside a git repo now. Copy contents exampleSite of theme:
```bash
cp -r themes/hugo-theme-relearn/exampleSite/* ./
```

Launch local web-server:
```bash
hugo server
```

Visit local site and check it.
## Gitlab sync
Create a Gitlab account, make SSH Key exchange. After that, create project by CLI from hugo site folder:

```shell
git branch -m master main # change master to main branch for gitlab
git push --set-upstream git@gitlab.com:aagern/hugotest.git main
```
Where hugotest.git is the project name. 

* Click **Setup CI/CD** button. 

Use Hugo template (should be in Gitlab):
```bash
---
# All available Hugo versions are listed here:
# https://gitlab.com/pages/hugo/container_registry
# image: "${CI_TEMPLATE_REGISTRY_HOST}/pages/hugo:latest"
image:
  name: "${CI_TEMPLATE_REGISTRY_HOST}/pages/hugo:latest"
  entrypoint: ["/bin/sh", "-c"]
variables:
  GIT_SUBMODULE_STRATEGY: recursive
test:
  script:
    - hugo
  except:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  environment: production
```

Change visibility: Settings -> General -> Pages visibility = Everyone.

