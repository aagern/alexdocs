+++
tags = ["devops", "script", "bash", "control", "config", "infrastructure" ]
description = "Alpine OS Config"
title = "Alpine Linux"
+++

## Installation on ESXi
[External Link](https://wiki.alpinelinux.org/wiki/Install_Alpine_on_VMware_ESXi)

* Enable Community repo in file /etc/apk/repositories
* Install: `apk add --update open-vm-tools`
* Enable at boot: 
    `rc-service open-vm-tools start`
    `rc-update add open-vm-tools`

## Network Config
[External Link](https://wiki.alpinelinux.org/wiki/Configure_Networking)
In file: /etc/network/interfaces

DHCP: `iface eth0 inet dhcp`

STATIC: 
```shell
iface eth0 inet static
        address 192.168.1.150/24
        gateway 192.168.1.1
```

#### DNS
In file: `/etc/resolv.conf`
```shell
nameserver 8.8.8.8
nameserver 8.8.4.4
```

## Configue CGROUPS for k8s
[External Link](https://wiki.alpinelinux.org/wiki/Docker)
`rc-update add cgroups` 

Bugfix:
```shell
echo "cgroup /sys/fs/cgroup cgroup defaults 0 0" >> /etc/fstab

cat > /etc/cgconfig.conf <<END
mount {
  cpuacct = /cgroup/cpuacct;
  memory = /cgroup/memory;
  devices = /cgroup/devices;
  freezer = /cgroup/freezer;
  net_cls = /cgroup/net_cls;
  blkio = /cgroup/blkio;
  cpuset = /cgroup/cpuset;
  cpu = /cgroup/cpu;
}
END

sed -i '/^default_kernel_opts/s/=.*$/="quiet rootfstype=ext4 cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory"/' /etc/update-extlinux.conf
update-extlinux
apk add --no-cache iptables curl
```

