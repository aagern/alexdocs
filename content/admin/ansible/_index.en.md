+++
tags = ["devops", "script", "bash", "control", "IaC", "config", "code", "infrastructure", "python" ]
description = "Ansible installation, config & usage"
title = "Ansible"
+++

### Articles in section
{{% children sort="weight" description="true" %}}

# Installation

Do not install Ansible from local OS repo - it is usually older in version there. Use latest Python pip repo to install a fresh version. 
```bash
sudo apt install python3-distutils
wget http://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py
sudo pip3 install ansible
ansible --version
```

{{% notice style="tip" %}}
Ansible will be the latest version supporting current Python in OS. So to get latest Ansible , Python must be updated as well!
{{% /notice %}}

{{% notice style="warning" %}}
Do not update the default Python in OS - it is used by system services, which may break!
{{% /notice %}}

Build Python:
```bash
sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev

wget https://www.python.org/ftp/python/3.9.15/Python-3.9.15.tgz

tar -xf Python-3.9.15.tgz

cd Python-3.9.15
./configure --enable-optimizations

make -j 8
```

Installing Python:
- Best way is to do **checkinstall**, create a **.deb** file to share with the team. 
- Use **altinstall** parameter to install Python in an alternative path. This is simpler & better than using Python virtualenv. 

```bash
sudo make altinstall # now Python 3.9x is installed on separate path, while Python 3.7 in OS is unchanged

sudo python3.9 -m pip install --upgrade pip

sudo python3.9 -m pip install ansible

ansible --version # now ansible is the latest version
```

# Architecture
```mermaid
graph LR
I[(Inventory)] --> R1(Role01);
M(Modules) --> R1;
I[(Inventory)] --> R2(Role02);
M(Modules) --> R2;
I[(Inventory)] --> RN(Role N);
M(Modules) --> RN;
R1 -->|include role| P(Playbook);
R2 -->|include role| P(Playbook);
RN -->|include role| P(Playbook);
P --> C(Ansible Config);
C --> Py(Python);
Py-->|SSH|Client01;
Py-->|SSH|Client02;
Py-->|SSH|Client_N;
```

## Folder Structure
![](folders.jpg)
video.yml - main program:
```yaml
---
- import_playbook: playbooks/db.yml
- import_playbook: playbooks/front.yml
- import_playbook: playbooks/zoneminder.yml
...
```
front.yml - role example:
```yaml
---
- name: front-end play
  hosts: all
  gather_facts: yes
  become: yes

  tasks:
    - name: include role apache
      include_role:
        name: apache

    - name: include role php
      include_role:
        name: php
...
```
apache / tasks / main.yml - tasks example:
```yaml
---
- name: install apache
  apt:
  name: apache2

- name: Enable service apache2
  ansible.builtin.systemd:
    name: apache2
    enabled: yes
    masked: no

- name: Make sure apache2 is running
  ansible.builtin.systemd:
    state: started
    name: apache2
...
```

### Role structure
Directories inside a role:
```
- defaults - variable values by default
-- main.yml

- vars - variables defined by role (for other roles)
-- main.yml

- tasks - jobs to be completed
-- main.yml

- handlers - actions to be taken after checks
-- main.yml

- files - static files to be copied into client machine
- templates
```