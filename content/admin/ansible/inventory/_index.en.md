+++
tags = ["devops", "script", "bash", "control", "IaC", "config", "code", "infrastructure", "python" ]
description = "Ansible inventory and config"
title = "Inventory"
+++

## Initialization
Create inventory file with IP addresses. Check computers in inventory by doing a ping:
```bash
ansible all -i inventory -u vagrant -m ping -k
# -i - <inventory file>
# -u - <username>
# -m - <select module>
# -k - <interactive password prompt>
ansible all -i inventory -u admin -m ping -k -vvv # debug mode with verbosity 3
```

### Inventory examples:
```bash
[db]
vm ansible_host=192.168.1.98 ansible_user=aagern

[app]
vm ansible_host=192.168.1.98 ansible_user=aagern

[front]
vm ansible_host=192.168.1.98 ansible_user=aagern

[all:vars]
ansible_ssh_extra_args="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
ansible_python_interpreter=/usr/bin/python3
```

```bash
web1 ansible_ssh_host=192.168.33.20
db1 ansible_ssh_host=192.168.33.30

[webservers]
web1

[dbservers]
db1

[datacenter:children]
webservers
dbservers

[datacenter:vars]
ansible_ssh_user=vagrant
ansible_ssh_pass=vagrant
```
## Configuration
Order of precedence for the config file:
1.  $ANSIBLE_CONFIG env variable  (first place to be searched)
2.  ./ansible.cfg - local file in the working directory
3.  ~/.ansible.cfg - user home directory
4.  /etc/ansible/ansible.cfg - global config (last place to be searched)

First config file found wins, Ansible stops looking for other config files.

### Environment override
Specify: $ANSIBLE_<config setting>

Override settings on the fly:
```bash
$ export ANSIBLE_FORKS=10
```
[defaults]
**forks** - how many parallel processes does Ansible handle. Default=5, production recommended=20

**host_key_checking** - check host key before sending commands. Default=True (for production), for dev/test systems =False (easy control)

**log_path** - Ansible logging. Default=Null, produstion recommended - set path all Ansible users can write to.

### Target patterns
Patterns to choose hosts/groups:
-   OR pattern: **group1:group2**

Example:
```bash
ansible webservers:dbservers -i inventory -m service -a "name=iptables state=stopped" --sudo
```
-   NOT pattern: **:!group2**
-   Wildcard pattern: **web*.lab.local**
-   REGEX pattern: **~web[0-9]+**
-   AND pattern: **group1:&group2** - apply to hosts BOTH in group1 and group2 (intersection)
-   Complex patterns

Complex pattern example: **webservers:&production:!python3** # apply to web servers in production but not in the Python3 group