+++
tags = ["devops", "script", "bash", "control", "IaC", "config", "code", "infrastructure", "python" ]
description = "Ansible playbooks, tasks"
title = "Playbooks"
+++

### General tasks usage
```yaml
# Tasks in a playbook are executed top down. Tasks use modules.
tasks:
  - name: Name the task for readability
    module: parameters=go_here

# Example:
  - name: Deploy Apache Configuration File
    copy: src=/ansible/files/configuration/httpd.conf

          dest=/etc/httpd/conf/
```

Playbook execution: `ansible-playbook my_playbook.yml`

Playbook example:
```yaml
---
# -------- Global play declaration
- hosts: webservers    
  ## ----- Variables per play
  vars:
    git_repo: https://github.com/repo.git
    http_port: 8081
    db_name: wordpress

  ## ------------------------
  ### ---- Declare user to run tasks
  sudo: yes
  sudo_user: wordpress_user

  ### ------------------------------
  gather_facts: no # dont't gather facts with SETUP module (default gathers facts - expensive in time)
  remote_user: root
  tasks:

# --------------------------------
  - name: Install Apache
    yum: name=httpd state=present
  - name: Start Apache
    service: name=httpd state=started
```

### Including files
Use "- include" and "- include_vars" directives to include playbook files:
```yaml
tasks:
- include: wordpress.yaml
  vars:
    sitename: My Awesome Site
    
- include: reverse_proxy.yaml
- include_vars: variables.yaml
```

### Register task output
Use the output of one task for another task:
```yaml
tasks:
- shell: /usr/bin/whoami
  register: username

- file: path=/home/myfile.txt
  owner: {{ username }}
```

### Debugging tasks with Debug module
Add screen output and print content of variables:
```yaml
tasks:
  - debug: msg="This host is {{ inventory_hostname }} during execution"

  - shell: /usr/bin/whoami
    register: username
    
  - debug: var=username
```

### Input during playbook execution
Promt user during execution:
```yaml
- hosts: web1
  vars_prompt:
  - name: "sitename"
    prompt: "What is the new site name?"

  tasks:
    - debug: var=username
```

### Playbook handlers
A handler can be informed to execute a task (restart service) only if state=changed.

-   Run after all tasks
-   Run only once no matter how many times they are called

Handlers syntax is the same as tasks syntax:
```yaml
  tasks:
  - name: Copy Apache Config files
  - copy: src=/files/httpd.conf
          dest=/etc/httpd/config/

    notify:
      - Apache Restart

  handlers:
  - name: Apache Restart
    service: name=httpd state=restarted
```

### "When" condition
Commence a task if condition is True:
```yaml
tasks:
- name: Install Httpd Package
  apt: name=httpd state=present
  when: ansible_os_family == "RedHat"

- name: Install Apache2 Package
  yum: name=apache2 state=present
  when: ansible_os_family == "Debian"
```

Check output of previous task as condition to run next task:
```yaml
tasks:
- name: Stop iptables now
  service: name=iptables state=stopped
  register: result
  ignore_errors: yes   # supress default stop on error

- debug: msg="Failure!"
  when: result|failed  # Debug message will only be shown if task has failed
                       # other conditions are "result|success", "result|skipped"
```

#### Checking variables with WHEN condition
Bring variable check to BOOL check:
```yaml
- name: "test"
  hosts: local
  vars_prompt:
    - name: "os_type"
      prompt: "What OS? (centos or ubuntu)"
      default: "centos"
      private: no
  vars:
    - is_ubuntu: "{{os_type == 'ubuntu'}}"
    - is_debian: "{{os_type == 'debian'}}"
  tasks:
    - debug: msg="this shows the conditional if variable equals ubuntu"
      when: is_ubuntu|bool
      
    - debug: msg="this shows the conditional if variable equals centos"
      when: is_centos|bool
```

