+++
tags = ["devops", "script", "bash", "control", "IaC", "config", "code", "infrastructure", "python" ]
description = "Ansible modules"
title = "Modules"
+++

### Help on modules
```bash
ansible-doc -l # all installed modules list
ansible-doc <module-name> # module man
ansible-doc -s <module-name> # playbook snippets code examples on module
```

## Module examples
### Copy module
-   Copies a file from the local box to a remote system - useful for copying config files to remote system
-   can do backups
-   can do remote validation

### Fetch module
-   Copy a file from remote system to local box
-   validate file using md5 checksum

### Setup module
-   Gather info and facts on remote system
-   Inventory analysis of the system
```bash
ansible -i inventory web1 -m setup # gather all available system info
ansible -i inventory web1 -m setup -a "filter=ansible_eth*" # gather info on NICs
ansible -i inventory all -m setup --tree ./setup # form an inventory of files in /setup/ folder with info on targeted systems
```
### Apt module (for Debian systems) / Yum module (RedHat systems)

-   Install, update, delete packages
-   Can update entire system
Example for Yum:
```bash
ansible webservers -i inventory -m yum -a "name=httpd state=present" -u vagrant --sudo
# name - name of package (Apache)
# present - if package is not there, install. If it is there - do nothing and report "changed: false" (idempotence test)
```
### Service module

-   Start/stop/restart services
-   Set autostart option for services
```bash
ansible webservers -i inventory -m service -a "name=httpd state=started enabled=yes" -u vagrant --sudo

# name - name of service (Apache)
# state = started/stopped - make idempotence test and change if necessary
# enabled = yes/no - autostart on system boot
```