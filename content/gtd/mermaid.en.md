+++
tags = ["GTD", "diagrams", "script", "schema" ]
description = "Drawing diagrams with Markdown"
title = "Mermaid"
+++

#### External Links
Tutorials: https://mermaid.js.org/config/Tutorials.html 

Online editor + exporter: https://mermaid.live

### Installation
Mermaid has plugins for VS Code, Obsidian, works with GitHub, GitLab etc. 

Full list of integrations: https://github.com/mermaid-js/mermaid/blob/develop/docs/misc/integrations.md

## Flowcharts
Example:
```markdown
graph LR  %% TD = Top->Down, LR = Left->Right etc.
S[Start] --> A;
A(Enter your EMail) --> E{Existing user?};
E -->|No| Create(Enter name)
E -->|Yes| Send(Send a letter to address)
Create --> EULA{Accept conditions}
EULA -->|Yes| Send
EULA -->|No|A
```

Result:
```mermaid
graph LR
S[Start] --> A;
A(Enter your EMail) --> E{Existing user?};
E -->|No| Create(Enter name)
E -->|Yes| Send(Send a letter to address)
Create --> EULA{Accept conditions}
EULA -->|Yes| Send
EULA -->|No|A
```
## Sequence Diagrams
Example:
```Markdown
sequenceDiagram
autonumber               %% action numbers placed on every arrow
actor C as Client
Note left of C: User     %% [ right of | left of | over ] supported
participant I as Identity Provider
participant S as Service Provider
Note right of S: Blitz Identity
C->>S: Resource request
activate C
activate S
S-->>C: Redirect to Identity Provider
deactivate S
loop every hour           %% loop start
C->>I: Request Access Token
activate C
activate I
I-->>C: Access Token
deactivate C
deactivate I
end                       %% loop end
C->>S: Access granted
Note over C,S: Browser access
deactivate C
```

Result:
```mermaid
sequenceDiagram 
autonumber
actor C as Client
Note left of C: User
participant I as Identity Provider
participant S as Service Provider
Note right of S: Blitz Identity
C->>S: Resource request
activate C
activate S
S-->>C: Redirect to Identity Provider
deactivate S
loop every hour
C->>I: Request Access Token
activate C
activate I
loop
I->>I: Kerberos cert
end
I-->>C: Access Token
deactivate C
deactivate I
end
C->>S: Access granted
Note over C,S: Browser access
deactivate C
```








