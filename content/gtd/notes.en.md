+++
tags = ["GTD", "diagrams", "script", "schema" ]
description = "Note taking with Obsidian"
title = "Obsidian Notes"
+++
## Общие советы по вводу текста
* Обязательно использовать слепой метод печати;
* Смена языка 1 клавишей - при слепом вводе лучше всего использовать caps lock;
* Плагины [Note refactor](https://github.com/lynchjames/note-refactor-obsidian) и [Better Link Inserter](https://github.com/salmund/obsidian-better-link-inserter) позволяют быстро создавать заметки из куска текста другой заметки и сразу создавать на неё ссылку;
* Плагин [templater](https://github.com/SilentVoid13/Templater) позволяет создавать быстрые шаблоны для записи содержимого.