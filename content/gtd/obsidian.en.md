+++
tags = ["GTD", "notes", "text", "obsidian" ]
description = "Configuring Obsidian"
title = "Obsidian"
+++
## Obsidian Articles
- [Obsidian для начинающих](https://youtu.be/OUrOfIqvGS4?si=HQWqR-UiiwgKHjM6)
- [10 маст хэв плагинов](https://www.youtube.com/watch?v=W7kTtn9empU)
- [Шаблоны заметок](https://www.youtube.com/watch?v=SUzRlhKFCYM&ab_channel=NicolevanderHoeven)
- [Как сделать дашборд](https://www.youtube.com/watch?v=AatZl1Z_n-g&ab_channel=FromSergio)
- [Как агрегировать информацию из множества заметок](https://www.youtube.com/watch?v=8yjNuiSBSAM&ab_channel=FromSergio)
- [Учёт прочитанных книг](https://www.youtube.com/watch?v=7PFFJlyiv28&ab_channel=FromSergio)
- [Как добавлять медиа в обсидиан](https://youtu.be/qt4g7Djy5AE?si=Jc5DD97v1yC11w7c)
- [Productivity | Zettelkasten в Obsidian](https://youtu.be/PiS3pRRj994?si=keX2uTYkgu_8EE5d)
- [Как использовать Tracker](https://www.youtube.com/watch?v=W_leEJHBZW4&ab_channel=FromSergio)
- [Как автоматизировать через Quick Add](https://www.youtube.com/watch?v=LrQVQ37y6IU&ab_channel=NicolevanderHoeven)
- [Как делать заметки из бумажных книг](https://www.youtube.com/watch?v=L9SLlxaEEXY)

Obsidian + Dataview
https://habr.com/ru/articles/710508/

Чтобы немного упростить процесс ведения дневника, можно поставить плагин календаря (Calendar), периодических заметок (Periodic notes) и шаблонов (Templater). Логика думаю тут довольно проста – календарь позволяет лучше ориентироваться во времени, периодические заметки - организуют заметки, а шаблоны упрощают создание самих дневниковых заметок.

Сначала мы быстро читаем какой-то отрывок, главу. Не отвлекаемся, не тормозим, не ходим по ссылкам.  
  
Далее читаем заново и оставляем метки, по которым сформируем впоследствии конспект.  
  
Пишем конспект:  
  
    Сначала пишем просто изложение (значит своими словами). Не думаем о ссылках, не думаем о том, как атомизировать. Просто пишем четкое последовательное изложение по прочитанному.  
  
    Расширяем и дополняем конспект своими мыслями и наблюдениями, вставляем ссылки и источники.  
  
    (при необходимости формируем какой-то сопроводительный текст)  
  
Начинаем атомизировать наш конспект на отдельные заметки.  
  
Связываем заметки с другими, если, конечно, в голову приходят эти связи.  
  
Медитируем

- Okular, потому что эта программа открывает pdf и djvu (и многие другие), а также в ней прям очень легко и быстро можно аннотировать текст.

#### Zotero

Это ультимативная, бесплатная программа для работы с источниками информации

- Obsidian to Anki.
### Плагины
- [ReadItLater](https://github.com/DominikPieper/obsidian-ReadItLater) - способ быстро добавлять статьи и видео в заметки;
- [Timestamp Notes](https://github.com/juliang22/ObsidianTimestampNotes) - смотреть видео и делать заметки по времени;
- [Templater](https://github.com/SilentVoid13/Templater) - заполнить пустую заметку по шаблону;
- [Various complement](https://github.com/tadashi-aikawa/obsidian-various-complements-plugin) - подсказки по словам при написании заметки;
- [Kanban](https://github.com/mgmeyers/obsidian-kanban) - канбан доска;
- [Dataview](https://github.com/blacksmithgu/obsidian-dataview) - можно сводить таблицы из разрозненных заметок;
- [Advanсed tables](https://github.com/tgrosinger/advanced-tables-obsidian) - удобная работа с markdown таблицами;
- [Tracker](https://github.com/pyrochlore/obsidian-tracker) - визуализация данных;
- [Quick Add](https://github.com/chhoumann/quickadd) - автоматизация процессов;
- [Homepage](https://github.com/mirnovov/obsidian-homepage) - плагин для создания домашних страниц;
- [Spaced Repetition](https://github.com/st3v3nmw/obsidian-spaced-repetition)- интервальное запоминание.

### Инструменты
- [Peerdraft](https://www.peerdraft.app/) - совместная работа над заметками;
- [Qartz](https://quartz.jzhao.xyz/) - расшарить в веб свой обсидиан;
- [Obsidian Publish](https://obsidian.md/publish) - тот же Кварц, но его не надо хостить, крутится на серверах Обсидиан (платный).
### Средства публикации заметок

- [Obsidian -> GitLab+Hugo](https://www.youtube.com/watch?v=-q6ZiCroiGM)
- [Obsidian -> Syncthing](https://youtu.be/6OKazb5PUj8?si=My9qfXaPO62S6GrG)
### Обсидианы других людей

Кварц позволяет открыть свои markdown-страницы для других людей через веб-сайт.

Есть даже целая концепция learn in public, когда люди открывают аудитории свои незавершённые наработки с целью эффективного обучения. Закон Каннингема гласит:

> Лучшим способом получить правильный ответ в Интернете будет не задавать вопрос, а разместить ложный ответ

Вот обсидианы некоторых инженеров:
- [Nicole Van Der Hoeven](https://notes.nicolevanderhoeven.com/Fork+My+Brain)
- [Simon Späti](https://www.ssp.sh/brain/)

Кстати, статьи, которые я пишу для своего [телеграм канала](https://t.me/mikevetkin) тоже написаны в Обсидиан и опубликованы через Quartz.  
  
Для того, чтобы их можно было удобно читать, не выходя из приложения, я написал шаблон для [telegram instant view](https://instantview.telegram.org/).
# Top Plugins
#### Calendar 
Позволяет создать календарь и писать в него быстрые заметки по дням.
#### DataView
Позволяет делать выборки из файлов с SQL Syntax и отображать результат в виде таблицы
#### Outliner
Создание и управление структурированными списками.
#### Novel Word Count
Отображение количества страниц и текста в папке.
#### Style Settings
Изменить стиль Obsidian, сделать Softpaper стиль.
Установить стиль (Theme) = **AnuPpuccin**
Зайти в настройки плагина Style Settings, выбрать конфиг для AnuPpuccin темы -> Import... и ввести:
```json
{
  "anuppuccin-theme-settings@@anuppuccin-theme-light": "ctp-rosepine-light",
  "anuppuccin-theme-settings@@anuppuccin-theme-dark": "ctp-frappe",
  "anuppuccin-theme-settings@@anuppuccin-light-theme-accents": "ctp-accent-light-teal",
  "anuppuccin-theme-settings@@anuppuccin-theme-accents": "ctp-accent-teal",
  "anuppuccin-theme-settings@@anuppuccin-accent-toggle": true,
  "anuppuccin-theme-settings@@ctp-custom-peach@@light": "#DD7F67",
  "anuppuccin-theme-settings@@ctp-custom-teal@@dark": "#11B7C5",
  "anuppuccin-theme-settings@@ctp-custom-teal@@light": "#1A7DA4",
  "anuppuccin-theme-settings@@ctp-custom-subtext1@@light": "#EE653A",
  "anuppuccin-theme-settings@@ctp-custom-subtext0@@dark": "#FB35D8",
  "anuppuccin-theme-settings@@ctp-custom-subtext0@@light": "#0C9FCE",
  "anuppuccin-theme-settings@@ctp-custom-overlay2@@dark": "#0AD1D0",
  "anuppuccin-theme-settings@@ctp-custom-overlay2@@light": "#525252",
  "anuppuccin-theme-settings@@ctp-custom-overlay1@@dark": "#FFA600",
  "anuppuccin-theme-settings@@ctp-custom-overlay1@@light": "#CCCCCC",
  "anuppuccin-theme-settings@@ctp-custom-overlay0@@dark": "#4CFFD2",
  "anuppuccin-theme-settings@@ctp-custom-overlay0@@light": "#0C9FCE",
  "anuppuccin-theme-settings@@anp-active-line": "anp-no-highlight",
  "anuppuccin-theme-settings@@anp-callout-select": "anp-callout-sleek",
  "anuppuccin-theme-settings@@anp-callout-color-toggle": true,
  "anuppuccin-theme-settings@@anp-custom-checkboxes": true,
  "anuppuccin-theme-settings@@anp-speech-bubble": true,
  "anuppuccin-theme-settings@@tag-radius": 2,
  "anuppuccin-theme-settings@@cards-border-width": "4px",
  "anuppuccin-theme-settings@@anp-color-transition-toggle": true,
  "anuppuccin-theme-settings@@anp-cursor": "pointer",
  "anuppuccin-theme-settings@@anp-toggle-scrollbars": true,
  "anuppuccin-theme-settings@@anp-editor-font-source": "\"\"",
  "anuppuccin-theme-settings@@anp-editor-font-lp": "\"New York\"",
  "anuppuccin-theme-settings@@bold-weight": "700",
  "anuppuccin-theme-settings@@anp-font-live-preview-wt": "400",
  "anuppuccin-theme-settings@@anp-header-color-toggle": true,
  "anuppuccin-theme-settings@@anp-header-divider-color-toggle": true,
  "anuppuccin-theme-settings@@h1-weight": 900,
  "anuppuccin-theme-settings@@h1-line-height": 1.2,
  "anuppuccin-theme-settings@@anp-h1-divider": true,
  "anuppuccin-theme-settings@@h2-size": 1.9,
  "anuppuccin-theme-settings@@h2-weight": 100,
  "anuppuccin-theme-settings@@h3-size": 1.6,
  "anuppuccin-theme-settings@@h3-weight": 700,
  "anuppuccin-theme-settings@@anp-h3-color-custom": "anp-h3-green",
  "anuppuccin-theme-settings@@h4-weight": 700,
  "anuppuccin-theme-settings@@h5-weight": 700,
  "anuppuccin-theme-settings@@h6-size": 1.1,
  "anuppuccin-theme-settings@@h6-weight": 700,
  "anuppuccin-theme-settings@@anp-decoration-toggle": true,
  "anuppuccin-theme-settings@@anp-colorful-frame": true,
  "anuppuccin-theme-settings@@anp-colorful-frame-opacity": 1,
  "anuppuccin-theme-settings@@anp-file-icons": true,
  "anuppuccin-theme-settings@@anp-file-label-align": "0",
  "anuppuccin-theme-settings@@anp-alt-rainbow-style": "anp-full-rainbow-color-toggle",
  "anuppuccin-theme-settings@@anp-rainbow-file-toggle": true,
  "anuppuccin-theme-settings@@anp-rainbow-folder-bg-opacity": 0.9,
  "anuppuccin-theme-settings@@anp-simple-rainbow-title-toggle": true,
  "anuppuccin-theme-settings@@anp-simple-rainbow-indentation-toggle": true,
  "anuppuccin-theme-settings@@anp-stacked-header-width": 30,
  "anuppuccin-theme-settings@@anp-alt-tab-style": "anp-safari-tab-toggle",
  "anuppuccin-theme-settings@@anp-alt-tab-custom-height": 40,
  "anuppuccin-theme-settings@@anp-disable-newtab-align": true,
  "anuppuccin-theme-settings@@anp-depth-tab-opacity": 0.6,
  "anuppuccin-theme-settings@@anp-depth-tab-gap": 10,
  "anuppuccin-theme-settings@@anp-safari-tab-radius": 5,
  "anuppuccin-theme-settings@@anp-safari-tab-gap": 3,
  "anuppuccin-theme-settings@@anp-safari-tab-animated": true,
  "anuppuccin-theme-settings@@anp-layout-select": "anp-card-layout",
  "anuppuccin-theme-settings@@anp-card-radius": 8,
  "anuppuccin-theme-settings@@anp-card-layout-padding": 1,
  "anuppuccin-theme-settings@@anp-card-shadows": true,
  "anuppuccin-theme-settings@@anp-card-layout-actions": true,
  "anuppuccin-theme-settings@@anp-card-layout-filebrowser": true,
  "anuppuccin-theme-settings@@anp-border-radius": 16,
  "anuppuccin-theme-settings@@anp-border-padding": 20
}
```