+++
archetype = "chapter"
title = "Getting Things Done"
weight = 4
+++

Everything around documenting, note taking, mind maps, schemes & diagrams.

{{% children sort="weight" description="true" %}}