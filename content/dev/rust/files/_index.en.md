+++
tags = ["programming", "code", "crossplatform" ]
description = "File reading in Rust"
title = "Files"
+++
## Получить список файлов
Функция read_dir:
```rust
use std::fs;
fn main() {
    let paths = fs::read_dir("./").unwrap();

    for path in paths {
        println!("{}", path.unwrap().path().display())
    }
}
```
## Получить список файлов с их абсолютным путём в системе
```rust
use std::fs;
use std::process;

fn main() {
    let paths = fs::read_dir("./").unwrap();

    for path in paths {
        let pathbuf = path.unwrap().path();
        println!("{:?}", fs::canonicalize(&pathbuf));
    }

    println!("\nPID = {}\n", process::id());
}
```
## Чтение текста из файлов в строку
Для чтения файлов, нужно сначала добавить несколько методов из библиотек: FIle - открытие файлов и Read - чтение, который входит в prelude. 
```rust
se std::fs::File;         // импорт File
use std::io::prelude::*;  // импорт Read
  
fn main() {  
    // читаемый файл находится в корне проекта.
    let mut file = File::open("access.log.10").expect("File not opened!");  
  
    let mut contents = String::new(); // закачать файл в переменную-строку
    file.read_to_string(&mut contents).expect("Cannot read file!");  
  
    println!("File contents: \n\n{}",contents);  
}
```
## Чтение в вектор из байтов (vector of bytes)
Чтение файла в память целиком как вектора байтов - для бинарных файлов, либо для частого обращения к  содержимому:
```rust
use std::io::Read;
use std::{env, fs, io, str};

fn main() -> io::Result<()> {
    let mut file = fs::File::open("test_file.txt")?;
    let mut contents = Vec::new();
    file.read_to_end(&mut contents);
    println!("File contents: {:?}", contents); // вывод байт

    let text = match str::from_utf8(&contents) { // перевод в строку UTF8
        Ok(v) => v,
        Err(e) => panic!("Invalid UTF-8: {e}"),
    };
    println!("Result: {text}"); // вывод строкой
    Ok(())
}
```
## Чтение текста из файла в буфер по строчкам
Нужно добавить к библиотеке File также библиотеку организации буфера чтения, а также обработать ошибки открытия файла и чтения. 
```rust
use std::io::{BufRead, BufReader};
use std::{env, fs, io};

fn main() -> io::Result<()> {
    let file = fs::File::open("textfile.txt")?;
    let reader = BufReader::new(file);
    
    for line in reader.lines() { // io::BufRead::lines()
        let line = line?;
        println!("{line}");
    }
    Ok(())
```
##  Запись в файлы
```rust
use std::fs::File;  
use std::io::prelude::*;  
  
fn main() {  
    let mut file = File::create("report.log").expect("File not created!");  
  
    file.write_all(b"report.log");  
}
```
