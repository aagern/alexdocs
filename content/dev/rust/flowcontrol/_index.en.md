+++
tags = ["programming", "code", "crossplatform" ]
description = "Rust program flow control - if-else, loops"
title = "Flow Control"
+++
## IF-ELSE
В Rust есть управление потоком программы через конструкции IF, ELSE IF, ELSE:
```rust
let test_number = 6;

if test_number % 4 == 0 {
println!("Divisible by 4");
} else if test_number % 3 == 0 { // Проверка останавливается на первом 
println!("Divisible by 3");      // выполнимом условии, дальнейшие проверки
} else if test_number % 2 == 0 { // пропускаются.
println!("Divisible by 2");
} else {
println!("Number is not divisible by 4, 3 or 2");
}
```

Конструкция IF является **выражением** (**expression**), поэтому можно делать присваивание:
```rust
let condition = true;

let number = if condition { "aa" } else { "ab" }; // присваивание результата IF
println!("Number is {number}");
```

## LOOPS
Три варианта организовать цикл: через операторы  **loop, while, for**.

LOOP - организация вечных циклов. Конструкция LOOP является **выражением** (**expression**), поэтому можно делать присваивание.
```rust
    let mut counter = 0;
    let result = loop {        
        counter += 1;
        if counter == 10 {
            break counter * 2; // выход из вечного цикла
        }
    }; // ";" нужно, т.к. было выражение
    println!("The result is {result}");
```

Если делать вложенные циклы, то можно помечать их меткой, чтобы выходить с break на нужный уровень. 
```rust
    let mut count = 0;
    'counting_up: loop {            // метка внешнего цикла
        println!("count = {count}");
        let mut remaining = 10;

        loop {
            println!("remaining = {remaining}");
            if remaining == 9 {
                break;
            }
            if count == 2 {
                break 'counting_up;  // goto метка
            }
            remaining -= 1;
        }

        count += 1;
    }
    println!("End count = {count}");
```

WHILE - цикл с условием. Можно реализовать через loop, но с while сильно короче. 
```rust
    let mut number = 10;
    while number != 0 {
        println!("{number}!");
        number -= 1;
    }
    println!("ЗАПУСК!");
```

FOR - цикл по множествам элементов. В том числе можно задать подмножество чисел.
```rust
for i in (1..10).rev() { // .rev() - выдача подмножества в обратную сторону
println!("Value: {i}");
}
println!("ЗАПУСК!");
```
## Функции
Вызов функции: указатель на начало функции кладётся на стек (под каждую функцию выделяется место на стеке - stack frame). Стек имеет ограничения по размеру. Можно это проверить, написав пример:
```rust
fn main() {
    a();
}

fn a() {
    println!("Calling B!");
    b();
}

fn b() {
    println!("Calling C!");
    c();
}

fn c() {
    println!("Calling A!");
    a(); // бесконечный цикл вызовов
}
```
При запуске программы она бесконечно вызывает функции, пока не исчерпает место в stack frame функции main, и тогда программа падает с ошибкой: 
```shell
thread 'main' has overflowed its stack
fatal runtime error: stack overflow
Abort trap: 6
```
