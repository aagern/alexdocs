+++
tags = ["programming", "code", "crossplatform" ]
description = "Constant values"
title = "Constants"
+++
## Константы
Это глобальные по области действия значения, неизменяемые. При объявлении нужно всегда сразу указывать их тип. 
```rust
const MAX_LIMIT: u8 = 15;  // тип u8 обязательно надо указать
  
fn main() {  
    for i in 1..MAX_LIMIT {  
        println!("{}", i);  
    }  }
```