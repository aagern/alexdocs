+++
tags = ["programming", "code", "crossplatform" ]
description = "Cross-compiling of RUST code to different OS"
title = "Cross-compiling"
+++

## Проверка системы
Сделать новый проект, добавить в него библиотеку `cargo add current_platform`. Далее создаём и запускаем код проверки среды компиляции и исполнения: 
```rust
use current_platform::{COMPILED_ON, CURRENT_PLATFORM};

fn main() {
    println!("Run from {}! Compiled on {}.", CURRENT_PLATFORM, COMPILED_ON);
}
```
* Посмотреть текущую ОС компиляции: `rustc -vV` 
* Посмотреть список ОС для кросс-компиляции: `rustc --print target-list` 
Формат списка имеет поля `<arch><sub>-<vendor>-<sys>-<env>`, например, `x86_64-apple-darwin` => macOS на чипе Intel. 

## Настройка кросс-компилятора
Нужно установить cross: `cargo install cross` установит его по пути `$HOME/.cargo/bin` 

Далее, нужно установить вариант Docker в системе: 
- На macOS => Docker Desktop;
- На Linux => [Podman](https://podman.io/)

Запуск кода на компиляцию под ОС Windows: `cross run --target x86_64-pc-windows-gnu` => в папке `target/x86_64-pc-windows-gnu/debug` получаем EXE-файл с результатом. 
❗Компиляция проходит через WINE. 

## Проверка среды компиляции
Cross поддерживает тестирование других платформ. Добавка проверки: 
```rust
mod tests {
    use current_platform::{COMPILED_ON, CURRENT_PLATFORM};

    #[test]
    fn test_compiled_on_equals_current_platform() {
        assert_eq!(COMPILED_ON, CURRENT_PLATFORM);
    } }
```
Запустить проверку локально: `cargo test`
Запустить проверку с кросс-компиляцией: `cross test --target x86_64-pc-windows-gnu`
На Linux/macOS проверка пройдёт, а вот при компиляции под Windows - нет:
`test tests::test_compiled_on_equals_current_platform ... FAILED
## Добавка платформенно-специфичного кода
Можно вписать код, который запустится только на определённой ОС, например, только на Windows:
```rust
use current_platform::{COMPILED_ON, CURRENT_PLATFORM};

#[cfg(target_os="windows")]
fn windows_only() {
    println!("This will only print on Windows!");
}

fn main() {
    println!("Run from {}! Compiled on {}.", CURRENT_PLATFORM, COMPILED_ON);
    #[cfg(target_os="windows")]
    {
        windows_only();
    }
}
```