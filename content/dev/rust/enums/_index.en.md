+++
tags = ["programming", "code", "crossplatform" ]
description = "Enums and match"
title = "Enums"
+++

## Перечисления
Тип перечислений позволяет организовать выбор из нескольких вариантов в пределах логического множества. 

```rust
enum Money {  
    Rub,  
    Kop  
}
```

## match
Аналог switch в других языках, однако, круче: его проверка не сводится к bool, а также реакция на каждое действие может быть блоком:

```rust
fn main() {  
    let m = Money::Kop;  
    println!("Я нашёл кошелёк, а там {}p",match_value_in_kop(m));  
}  
  
fn match_value_in_kop(money: Money) -> u8 {  
    match money {  
        Money::Rub => 100,  
        Money::Kop => {  
            println!("Счастливая копейка!");  
            1  
        }  }  }
```

Проверка условия и запуск соответствующего метода:
```rust
struct State {
    color: (u8, u8, u8),
    position: Point,
    quit: bool,
}

impl State {
    fn change_color(&mut self, color: (u8, u8, u8)) {
        self.color = color; }

    fn quit(&mut self) {
        self.quit = true; }

    fn echo(&self, s: String) {
        println!("{}", s); }

    fn move_position(&mut self, p: Point) {
        self.position = p; }

    fn process(&mut self, message: Message) {
        match message { // проверка и запуск одного из методов
            Message::Quit => self.quit(),
            Message::Echo(x) => self.echo(x),
            Message::ChangeColor(x, y, z) => self.change_color((x, y, z)),
            Message::Move(x) => self.move_position(x),
        } } }
```

## if let
В случае, когда выбор сводится к тому, что мы сравниваем 1 вариант с заданным паттерном и далее запускаем код при успехе, а в случае неравенства ничего не делаем, можно вместо match применять более короткую конструкцию if-let:
```rust
    let config_max = Some(3u8);
    match config_max {
        Some(max) => println!("The maximum is configured to be {}", max),
        _ => (), //  другие варианты ничего не возвращают
    }
```
Превращается в:
```rust
    let config_max = Some(3u8);
    if let Some(max) = config_max {
        println!("The maximum is configured to be {}", max);
    }
```

Применение if-let - это синтаксический сахар, укорачивает код, однако, лишает нас проверки наличия обработчиков на все варианты возвращаемых значений как в конструкции match.

## Сравнение величин
Для сравнения значений в переменных есть метод **std::cmp**, который возвращает объект типа enum **Ordering** с вариантами:
```rust
use std::cmp::Ordering;
use std:io;

fn main() {
let secret_number = 42;
let mut guess = String::new();
io::stdin()
.read_line(&guess)
.expect("Read line failed!");
  
let guess :i32 = guess.trim().parse()
  .expect("Error! Non-number value entered.");

match guess.cmp(&secret_number) {
  Ordering::Greater => println!("Number too big"),
  Ordering::Less => println!("Number too small"),
  Ordering::Equal => println("Found exact number!")
 }
}
```