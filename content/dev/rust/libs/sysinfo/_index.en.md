+++
tags = ["programming", "code", "crossplatform" ]
description = "System Information Library"
title = "sysinfo"
+++

Библиотека для изучения параметров системы и системных ресурсов. 
https://docs.rs/sysinfo/latest/sysinfo/ 
## Установка
```shell
cargo add sysinfo
```
## Использование
```rust
use sysinfo::{RefreshKind, System};

fn main() {
    let _sys = System::new_with_specifics(RefreshKind::nothing());
    println!("System name:             {:?}", System::name());
    println!("System kernel version:   {:?}", System::kernel_version());
    println!("System OS version:       {:?}", System::os_version());
    println!("System host name:        {:?}", System::host_name());
}
```

С помощью `RefreshKind::nothing()` отключаются все динамические вещи, такие как остаток свободной памяти, загрузка ЦПУ, сеть и так далее, что драматически ускоряет опрос системы. 

Можно частично опрашивать разные системные ресурсы, по заданным интервалам. 