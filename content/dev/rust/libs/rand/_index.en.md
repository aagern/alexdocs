+++
tags = ["programming", "code", "rust" ]
description = "Random numbers library"
title = "rand"
+++

## Generating random numbers

External links: 
* [Rust Rand Book](https://rust-random.github.io/book/);
* [rand lib](https://crates.io/crates/rand);
* [rand lib doc](https://docs.rs/rand/);

Using rand lib example:
```rust
use rand::Rng;

fn main() {
let secret_of_type = rand::rng().random::<u32>();
let secret = rand::rng().random_range(1..=100);

println!("Random nuber of type u32: {secret_of_type}");
println!("Random nuber from 1 to 100: {}", secret); }
```

В старой версии библиотеки применялся признак gen(), который переименовали в связи с добавлением gen() в Rust 2024. 

