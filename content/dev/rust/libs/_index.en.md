+++
tags = ["programming", "code", "rust" ]
description = "Rust libraries"
title = "Libraries"
weight = 1
+++
## Articles in Section

{{% children sort="weight" description="true" %}}
## Library Unit-tests
Примеры:
```rust
pub fn greet_user(name: &str) -> String {
    format!("Hello {name}")
}

pub fn login(username: &str, password: &str) -> bool {
    username == "admin" && password == "P@ssw0rd"
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_greet_user() {
        assert_eq!("Hello Alex", greet_user("Alex"));
    }

    #[test]
    fn test_login() {
        assert!(login("admin", "P@ssw0rd"));
    }
}
```

Запуск тестов - командой `cargo test`