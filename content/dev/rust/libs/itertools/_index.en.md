+++
tags = ["programming", "code", "rust" ]
description = "Itertools library"
title = "itertools"
+++

External link: https://docs.rs/itertools/latest/itertools/
## Working with iterators

### Sorting() a string of letters (with rev() - reverse order)
```rust
use itertools::Itertools;

let text = "Hello world";
let text_sorted = text.chars().sorted().rev().collect::<String>();
// rev() - Iterate the iterable in reverse
   
println!("Text: {}, Sorted Text: {}", text, text_sorted); 
// Text: Hello world, Sorted Text: wroollledH 
```

### Counts() подсчёт количества одинаковых элементов в Array
```rust
use itertools::Itertools;

let number_list = [1,12,3,1,5,2,7,8,7,8,2,3,12,7,7];
let mode = number_list.iter().counts(); // Itertools::counts() 
// возвращает Hashmap, где ключи взяты из массива, значения - частота
for (key, value) in &mode {  
    println!("Число {key} встречается {value} раз");  
 } 
```

