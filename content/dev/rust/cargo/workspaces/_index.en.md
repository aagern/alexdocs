+++
tags = ["programming", "code", "crossplatform" ]
description = "Cargo Project Workspaces"
title = "Cargo Workspaces"
+++
# Cargo Workspaces
Объединение кода в 1 проект. В Cargo.toml нужно объявить его структуру:
```toml
[package]
name = "ardan_workspace"
version = "0.1.0"
edition = "2021"

[dependencies]

[workspace]
members = [ 
  "session1/hello_world",
  "session1/variables",
]
```
После этого можно добавлять подпрограммы в members, через `cargo new <подрограмма>`.
## Создание и подключение библиотеки в Workspace
Создание библиотеки вместо бинарника - `cargo new --lib examplelib

Прописать в файле cargo.toml у бинарной программы в workspace зависимость: 
```toml
<..>

[dependencies]
examplelib = { path = "../examplelib"}
```

Далее, в код бинарной программы включить функции из библиотеки: 
```rust
use examplelib::function01; // фукнкция должна быть публичной (pub fn)
```
