+++
tags = ["programming", "code", "crossplatform" ]
description = "Cargo Offline"
title = "Cargo  Offline"
+++
# Cargo OFFLINE
Для создания локального сервера можно скачать все пакеты Cargo с помощью проекта [Panamax](https://github.com/panamax-rs/panamax). 
```bash
cargo install --locked panamax
panamax init ~/test/my-mirror
```
Нужно зайти в папку my-mirror, проверить настройки в файле mirror.toml. И далее запустить синхронизацию:
```shell
panamax sync ~/test/my-mirror
```
Далее, можно публиковать зеркало по веб через встроенный сервер (по порту 8080):
```shell
panamax serve ~/test/my-mirror
```
 На индекс странице сервера будет справка по подключению Rust клиентов к зеркалу. В частности, посредством создания файла настроек ~/.cargo/config :
```shell
[source.my-mirror]
registry = "http://panamax.local/crates.io-index"
[source.crates-io]
replace-with = "my-mirror"
``` 