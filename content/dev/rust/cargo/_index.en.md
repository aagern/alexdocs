+++
tags = ["programming", "code", "crossplatform" ]
description = "Cargo"
title = "Cargo"
weight = 1
+++

Use Cargo for managing projects. 
```bash
cargo new test_project // create project with binary file src/main.rs
// OR
cargo new test_project --lib // create project with library file src/lib.rs 

cd test_project
```
Source code is in **src** folder. 
```bash
cargo build # build project for debug, do not run
cargo run # build & run project
cargo check # fast compiling
cargo build --release # slow build with optimizations for speed of release version
```

Documentation of methods & traits used in code can be compiled an opened in browser with Cargo command:
```shell
cargo doc --open
```
### Cargo Examples
Create "examples" folder beside the "src" folder. Create a file named, for example, "variables.rs" and place some test code in the folder. Then in the project root folder run:
```bash
cargo run --example variables
```
### (new) Bacon - интерактивный перезапуск компиляции
This will compile+build the code in examples folder, file "variables.rs". Very convenient to try test different stuff. For live development do:
```bash
bacon run -- -q # отслеживать текущий проект
bacon run -- -q --example <файл> # отслеживать файл в папке examples
```
-q - убрать вывод деталей компиляции
### (old) Cargo Watch - интерактивный перезапуск компиляции
This will compile+build the code in examples folder, file "variables.rs". Very convenient to try test different stuff. For live development do:
```bash
cargo watch -q -c -x 'run -q --example variables'
```
-x - rerun upon code change
-c - clear terminal each time
-q - quiet mode
**❗Проект cargo watch заморожен, более не развивается.** 
### Panic Response
В ответ на панику, по умолчанию программа разматывает стек (unwinding) - проходит по стеку и вычищает данные всех функций. Для уменьшения размера можно просто отключать программу без очистки - abort. Для этого в файле **Cargo.toml** надо добавить в разделы `[profile]`:
```toml
[profile.release]
panic = 'abort'
```
## Cargo Clippy linter
Example of Clippy config:
```shell
cargo clippy --fix -- \
-W clippy::pedantic \
-W clippy::nursery \
-W clippy::unwrap_used \
-W clippy::expect_used
```

Clippy has a markdown book:
```shell
cargo install mdbook
# Run from top level of your rust-clippy directory:
mdbook serve book --open
# Goto http://localhost:3000 to see the book
```
## Important Cargo libs
- Tokio - async runtime
- Eyre & color-eyre - type signatures, error report handling
- Tracing - collect diagnostic info
- Reqwest - HTTP requests library, async
- Rayon - data parallelism library, without data races
- Clap - commandline passing library
- SQLX - compile-checked SQL, async
- Chrono - time/date library
- EGUI - web-gui @60FPS, runs in Webassembly
- Yew.rs - web-library like React.js
