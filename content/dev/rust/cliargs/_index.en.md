+++
tags = ["programming", "code", "crossplatform" ]
description = "Command-line arguments"
title = "CLI Arguments"
+++

## ARGS
Аргументы командной строки можно захватить с помощью методов args()+collect() библиотеки env. Нужно поместить их в строковый вектор.

```rust
use std::env;  
  
fn main() {  
    let args: Vec<String> = env::args().collect();  
  
    for arg in args.iter() {  
        println!("{}", arg);  
    } }
```

Первым параметров всегда идёт имя самой программы. 

{{% notice style="tip" %}}
Продвинутую обработку аргументов CLI удобно делать с помощью [библиотеки Clap](../libs/clap/index.html)
{{% /notice %}}