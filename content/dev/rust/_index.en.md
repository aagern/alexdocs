+++
tags = ["programming", "code", "crossplatform" ]
description = "Rust language notes"
title = "Rust"
+++
## Articles in Section

{{% children sort="weight" description="true" %}}
### Аргументы за/против Rust
Плюсы Rust: 
* Memory safety, высокая скорость приложений +самые быстрые в мире веб-фреймворки https://www.techempower.com/benchmarks/#hw=ph&test=fortune&section=data-r22
* Высокая скорость вычислений для ML, Python переходит под капотом на Polars взамен Pandas - https://rongxin.me/substitute-pandas-with-polars-a-dataframe-module-rewritten-in-rust
* любимый язык StackOverflow уже 8+ лет https://survey.stackoverflow.co/2024/technology/#admired-and-desired
* Ядро [Linux](https://en.wikipedia.org/wiki/Rust_for_Linux), ядро Android, ядро [Windows](https://www.techspot.com/news/98492-microsoft-rewriting-windows-rust-sort.html), ядро контейнеров переезжает с Golang
* Проект переписывания всех главных системных утилит Linux на Rust - https://github.com/TaKO8Ki/awesome-alternatives-in-rust
* Fearless Concurrency
* Диалог с компилятором
* Передача переменных в функции максимально чётко
* нет void / null https://www.youtube.com/watch?v=ybrQvs4x0Ps
* мощная типизация
* traits вместо классов и наследования
* Единый сборщик Cargo
* На выходе единый бинарник как в Golang
* Нативная поддержка WebAssembly (за счёт отсутствия сборщика мусора), [первый язык](https://www.infoworld.com/article/2335881/rust-is-most-popular-webassembly-language-survey-says.html)
* Rust главный язык надёжной разработки финансовых инструментов для [технологий Blockchain](https://www.itmagination.com/blog/rust-development-blockchain-web3)

Минусы:
- Его сложно учить, много синтаксиса
- Перестроение мозгов: дуализм результата (enums Result), метки lifecycle, traits
- Язык молодой, экосистема беднее чем у Python, тем более чем у C++, сообщество меньше
#### Сравнения
Golang, хороший-плохой-злой: https://habr.com/ru/companies/vk/articles/353790/
## Видеокурсы
- Курс Алексея Кладова - https://youtu.be/Oy_VYovfWyo?si=WRCHJv_0cZYitGao 
## Задачи по программированию
+ https://www.codewars.com/ 
+ https://leetcode.com/
+ Свежая статья по теме - https://habr.com/ru/articles/726366/ 
+ Интерактивные задачи на ветвления в Git - https://learngitbranching.js.org/
## Rustup
После установки Rust через rustup можно переключать версии компилятора: 
```shell
rustup show # посмотреть текущие установленные варианты rustup

rustup install nightly
rustup install beta
rustup install stable

rustup default nightly
rustup default stable
```
## Rust Tools
External links: 
* https://youtu.be/ifaLk5v3W90
* https://youtu.be/kQvMPfR7OwE
* Example of WebAssembly + Rust = https://makepad.dev/
- Rustup toolchain installer - https://rustup.rs/ 
- Tokei Lines of code count - https://github.com/XAMPPRocky/tokei 
- Rustup add-ons needed for formatting and as dependencies:
```bash
rustup component add rustfmt 
rustup component add rust-src
```
- Install Bacon (formerly Cargo-watch) to rerun code upon save:
```bash
cargo install --locked bacon
#cargo install cargo-watch
```
- Rust-analyzer - `brew install rust-analyzer`
- Astro Vim for fast IDE, upgrade on NVIM - https://astronvim.github.io 
```shell
git clone https://github.com/AstroNvim/AstroNvim ~/.config/nvim
nvim +PackerSync

# After install use commands:
:LspInstall rust -> rust-analyzer
:TSInstall rust
```
- Neovide GUI upgrade on Astro Vim
```shell
git clone https://github.com/neovide/neovide
cd neovide
cargo build --release
```
- EVCXR or iRUST REPL
```shell
cargo install evcxr_repl
cargo install irust
```
- Bacon terminal-based test runner (https://dystroy.org/bacon/)
```shell
cargo install --locked bacon
bacon test # run from project folder 
```
- Lazygit (https://github.com/jesseduffield/lazygit#homebrew)

## VSCode Extensions for Rust
- CodeLLDB
- Dependi (бывш crates) - сообщает, если пакеты устарели
#### VSCode Settings
(For CodeLLDB) Allow breakpoints everywhere: `"debug.allowBreakpointsEverywhere": true`
cargo check: поменять check на clippy: `"rust-analyzer.check.command": "clippy"`
### Rust Prelude
Rust has a Prelude - a set of libraries included in every project. 
See [current libs included in Prelude](https://doc.rust-lang.org/stable/std/prelude/index.html) 
### Allow Unused
Turn off noise about unused objects while learning with a crate attribute:
```rust
#![allow(unused)] // silence unused warnings while learning
fn main() {}
```
### User input
std::io::stdin library is used to get user input from standard input stream. Not included in Prelude:
```rust
use std:io

let mut guess = String::new();
io::stdin().read_line(&mut guess).expect("Failed to load");
```
**.expect** handles **Err** variant of **read_line** function, crashing the program with the defined error message.


