+++
tags = ["programming", "git", "repository", "code", "devops", "version", "control", "script" ]
description = "Using Git repository"
title = "Git"
+++
## Configuring Git
```bash
git config --global user.name "spider_net" # username init, seen in commits
git config --global user.email "test@gmail.com" # Init of user e-mail
```
## SSH одновременно с GitHub и Gitlab
Для работы с одной системы с тем и другим репозиторием по SSH нужно создать ключи SSH под оба, а также прописать в файле конфигурации профили подключения:
```ini
Host github.com
  IdentityFile ~/.ssh/id_rsa
  IdentitiesOnly yes

Host gitlab.com
  IdentityFile ~/.ssh/id_ecdsa
```
Для проверки работоспособности конфигурации можно подключиться с помощью ssh напрямую:
```bash
ssh -vvvv git@github.com
```
Для избежания ошибок следует:
- В имени ключа не использовать знак тире `-`
- Сначала клонировать удалённый репозиторий с помощью `git clone`, далее локально его обновить и синхронизировать с помощью `git push origin main` 
### Cloning Repository
```bash
git clone https://github.com/drupal/drupal.git drupal7
```
## Initialising Local & Remote Repo
```bash
git init # local repo init

git status # check status

git branch -m master main # change master branch to main, for GitLab

git add * # add all files to repo, .gitignore file lets you hide junk files, password files from syncing

git commit -m "Hello world"

git config --global user.email "Ivan@example.com"
git config --global user.name "Ivan the Terrible"

git log

git remote add origin git@gitlab.com:<name of account>/<project name>.git

git push origin main
```
### Cloning Remote Repo on Amazon
Certificate file needed in `.ssh` folder. Also a config file .ssh/config is needed:
```ini
Host git-codecommit.us-east-1.amazonaws.com
   User Your-SSH-Key-ID, such as APKAEIBAERJR2EXAMPLE
   IdentityFile Your-Private-Key-File, such as ~/.ssh/codecommit_rsa or     ~/.ssh/id_rsa
```

Git commands to clone repo:
```bash
git clone ssh://<SSHKEY-ID>@git-codecommit.<REGION>.amazonaws.com/v1/repos/<REPO-NAME>

# Example:

git clone ssh://ABCDOIJKHJGYTHJG7@git-codecommit.eu-central-1.amazonaws.com/v1/repos/myrepo
```
## Branching
Create new branch: `git branch new-branch`
Pointer does not automatically switch to new branch, it is left on main branch, so you need to switch manually: `git checkout new-branch`
### Merging Branches
- Switch to branch for merging
- Merge
- Check pointers point to one place OR merge the other branch with the current one
```bash
git commit
git checkout main
git commit
git merge new-branch
```

Change of branches to make it look as if the functionality was developed step-by-step, not in parallel.
![](rebase.jpg)
```bash
git rebase master
git checkout master
git rebase bugFix
```
### Pointer 
HEAD is the pointer to current project state. By default HEAD points to current branch. 
You can go back by commits or by direct commit hash:
`git checkout C1`

You can use syntax "^K" for going up 1 level (where K is the route number to choose if there are more than 1 parent. By default, K=1) and "~N" for N steps up:
```bash
git checkout master^
# OR
git checkout bugFix^^
# OR
git checkout bugFix~2
# same route on schema from picture
# or build a sequence
git checkout bugFix^~1
```

Chaning main branch to several commits backwards:
```bash
git branch -f master HEAD~3
# -f means force - direct remap of branch for commit
```
![](movebranch.jpg)
### Undo Actions
For 1-person local repo do reset. 
For multi-person remote repo, do revert. 
```bash
git reset HEAD~1
# OR
git revert HEAD
```
![reset](reset.jpg)
![revert](revert.jpg)
In c2' state are the changes, which cancel state C2
### Moving commits
Chosen commits are moved upon current commit using **cherry-pick** command. Used when it is known what specific commits to move: `git cherry-pick C2 C4`
![](cherry_pick.jpg)
`git rebase -i HEAD~4 --aboveAll`
![](rebase_i.jpg)
### Small commit modifications
You can modify a commit, dividing it into two with same parent:
`git commit --amend`
### Tagging commits
Marking milestones is done with tags. They block the commit from changes, moving etc.:
```bash
git tag v1 <hash> # if hash is not provided, then current commit is tagged
```
Tags serve as anchors in tree of commits. To define your position against the nearest anchor, use command:
```bash
git describe <ref> # if <ref> is not provided, current commit will be described
```
## Naming
Remote branches have a naming convention: **`<remote name>/<branch name>`**

Main remote i called **origin**. So master branch is **origin/main**. When doing commit in local branch, the system is put into detached HEAD mode:
```bash
git checkout origin/master
git commit
```

![](remotemaster.jpg)
## Fetch data from remote repository
When data is downloaded from remote repo, the origin/main branch is updated to reflect the new commits:
```git fetch```

Only commits non-present locally are downloaded. No local state of files is changed after download. The local main status is unchanged. To change, a merge must be done:
**git fetch + git merge = git pull**
`git pull`
## Publishing local repository to remote
Publishing syncs commits at remote repo and local repo (main and origin/main point to the same commit):
`git push`

If the remote repo has changed by someone by the time you need to push there, it means that your feature is based on an old commit. So Git will not let you push. Before push, a fetch must be made to sync the changes, than a rebase or merge to update the main branch, and then a push:

**git fetch + git rebase origin/main + git push** OR **git fetch + git merge origin/main + git push** = **git pull --rebase**
`git pull --rebase`
### Changing main and origin/main
You can change that another branch will be main for the remote repo: 
`git checkout -b side origin/main
## Push arguments
You can specify what branch to push:
`git push origin main`

origin = remote repo, 
main = branch to take commit from.

It does not matter where the HEAD is at this moment. You can specify where to push commits using ```git push origin <source>:<destination>``` notation:
`git push origin side^2:master`

If push is made to a non-existent branch on remote repo, Git will create this branch: 
`git push origin main:newBranch`
## Fetch arguments
Same as **push**, but the other wat around. Go to foo branch on remote **repo**, download commits from there to local branch origin/foo:
`git fetch origin foo`