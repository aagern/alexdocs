+++
tags = ["darktable", "photo", "art", "foto", "chapter"]
description = "Using Darktable Photo Editor"
title = "Darktable"
+++

## Denoise with Wavelets
- Use the **Denoise (Profiled)** module (type "denoise" in search bar);
- Default options - eliminate chroma noise; 
- Increase **Strength** = 1.1-1.2 if needed (not too much).
- Create a second instance of **Denoise (Profiled)** module;
- Turn **mode=non-local means** instead of wavelets to remove grain;
- Select **Drawn Mask** to expand the preferences;
- Click **Add Brush** button and draw a mask on the parts of the main object in focus;
![](brush.jpg)
- Click **Toggle polarity of drawn mask** (Invert mask) button;
- The mask can be seen using **Display mask** button;
- Increase **feathering radius** and **blurring radius** values;
- Turn down the **Strength** to the minimal viable =~0.5-0.6;
![](brushmask.jpg)
- Applying Denoise might lead to an "oil painting" effect. To fix this, add a **Contrast Equalizer** module; 
- Click on **Drawn Mask** button, choose **drawn mask = denoise (profiled)**;
- Click **Toggle polarity of drawn mask** (Invert mask) button;
- The mask can be seen using **Display mask** button;
- Increase **feathering radius** and **blurring radius** values;
- Create a **luma** graphic curve in the **fine** section to smooth the oil effect:
![](contrasteq.jpg)

