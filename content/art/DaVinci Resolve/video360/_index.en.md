+++
tags = ["davinci", "resolve", "video", "audio"]
description = "Editing 360VR Video"
title = "VR360"
+++

External links: 
* https://youtu.be/ypB2J_p1Ayk (Base Edit & view)
* https://youtu.be/CWw2DaXC7OU (ReFrame)
* https://youtu.be/8u7GY436_Jw (Animate 3D Text)
* https://youtu.be/xlOhluai5mk (Stabilization & hide tripod)

## Подготовка видео
Для Insta360 X2 - видео 6K@30FPS:
* Собрать видео и экспорировать из родной программы в 360VR-ProRes 422 формат.  
Поставить галочки **FlowState**, **Lock Direction** (для видео с рук или с дрона, для видео со штатива не надо).
* Очистить видео от артефактов и увеличить до 8K. С помощью Topaz Video AI сделать 2 прохода: 
    * Первый проход с алгоритмом Artemis DeHalo, размер 100%, добавить Grain (Size=1);
    * Второй проход с алгоритмом Gaia High Quality, размер 133,33%.

## Подготовка проекта
Разрешение:
* 3840x1920 - для соц сетей, интерактивного просмотра с моб телефонов подойдёт 
* 5760х2880 - для YouTube 6K показа
* 7680x3840 - для Youtube 8K показа

Проксирование (Optimized Media and Render Cache):
* Proxy media resolution = **Quarter**
* Кодек = на Apple это **Prores 422**
* Указать папку для складывания видео Proxy. 

## Просмотр VR360
Положить видео VR360 на timeline. Далее перейти в комнату Fusion, нажать правой кнопкой на видео в окне и выбрать **360 View -> LatLog**. После этого можно панорамировать с помощью **ALT+средняя кнопка мышки**. 
![](VR360_01.jpg)
Когда закончил панормирование, можно нажать правой кнопкой на видео в окне и выбрать **Settings -> Reset Defaults** 

Для ускорения просмотра выбрать режим отображения Proxy и отключить высокое качество.
![](VR360_07.jpg)
Также в главном меню выбрать **Playback -> Timeline Proxy Resolution -> Half**.

## Смена направления на север и завал горизонта
Для начальной ориентации камеры нужно добавить ноду **PanoMap**. Далее перевести отображение обоих окон с видео на эту ноду. Выбрать просмотр в одном из окон **360 View -> LatLog**. С помощью настроек **PanoMap** поменять ориентацию по осям.  
![](VR360_06.jpg)
При съёмке с дрона может быть завал горизонта: исправить можно путём анимации осей. XYZ в настройках **PanoMap**. 

## Редактировать - убрать штатив/дрона
* Добавить в Fusion ноду Lat Long Patcher. По оси X направить чётко вниз/вверх, чтобы было видо основание штатива или дрон. 
![](VR360_02.jpg)

* Добавить ноду **Paint**. Выбрать в окне видео **Stroke**, в настройках ноды **Paint** выбрать режим **Clone**, размер кисти большой и повысить гладкость. С помощью **ALT** обозначить точку для клонирования и закрасить крестовину штатива или дрон.
![](VR360_03.jpg)
Скопировать ноду **Lat Long Patcher** после **Paint**, поменять у неё в настройках режим **Mode**=**Apply**. После этого добавить ноду **Merge** и соединить в неё вторую ноды **MediaIn**, **Lat Long Patcher** и результат вывести на **Media Out**. Объект должен пропасть в кадре. 
![](VR360_04.jpg)

## Стабилизация VR360
В команте Fusion добавить ноду **Spherical Stabilizer**. **Stabilize Strength = 1**, **Smooting = 1**. Произвести трекинг всего видео. 
![](VR360_05.jpg)

## Создание объектов в 3D видео
* Добавить ноды **Spherical Camera, Merge 3D, Renderer 3D, Shape 3D**;
* У ноды **Shape 3D** установить **Shape = Sphere** и соединить в неё **Media In**. 
![](VR360_08.jpg)
* Подключить вывод видео в окно просмотра в ноде **Merge 3D**;
* Можно панорамировать в окне просмотра с помощью **ALT+средняя кнопка мышки**;
* У ноды **Shape 3D** установить **Radius = 10**, чтобы "войти в сферу", увеличить **Base Subdivisions = 60** и выше, чтобы убрать полигональные артефакты;
* Выделить ноду **Render 3D**, включить отображение видео в 1 окно. Обратить внимание, что точка обзора поменялась. Нажать **CTRL+G**, чтобы отобразить перекрестье и направляющие;
* У ноды **Spherical Camera** во вкладке **Transform** по оси **Y** подвинуть ось камеры, чтобы установить её в нужном направлении;
![](VR360_09.jpg)
* Проверить, что включено низкое качество показа, Proxy, в главном меню выбрать **Playback -> Timeline Proxy Resolution -> Quarter** для ускорения Renderer 3D;
* Добавить ноду Text 3D, написать текст, подключить к ноде Merge 3D;
* Использовать направляющие перемещения и поворота для правильной ориентации текста по отношению к камере;
![](VR360_10.jpg)
* Перейти в настройках **Text 3D** во вкладку **Transform**, добавить ключи анимации по осям XYZ;
* Включить в интерфейсе Fusion вкладку **Spline**, включить в ней все оси анимации, нажать **CTRL+F** для масштабирования графиков анимации в экране; 
* Сделать анимацию по времени, нажать на ключевые кадры и усиками Безье придать плавность анимации;
![](VR360_11.jpg)
* Соединить выход Renderer 3D с Media Out;

### Отображение 2D текста и графики в 3D
* Добавить ноды **Text, Background**, соединить их через ноду **Merge**, после чего внести в 3D через ноду **Image Plane 3D**;
* В настройках Image **Plane 3D -> Transform** регулировать масштаб **Scale = 3**;
![](VR360_12.jpg)
* Можно таким же образом перестаскивать из Media Pool обычные видеоролики и подключать через Image Plane 3D.

## Экспорт видео
Для 6К - 8К видео выбирать кодек H.265.
Битрейт **Quality = 100000 Kb/s** для большей плавности картинки. 
{{% notice style="warning" %}}
Для шлема Oculus Quest 1 нужно указывать не более чем **Quality = 60000 Kb/s**. 
{{% /notice %}}

Включить **Advance Settings -> Force Sizing to highest quality, Force debayer to highest quality**. 

DaVinci Resolve не делает инъекции метаданных в видео, поэтому для публикации на Youtube нужно [это сделать отдельно](https://github.com/google/spatial-media/tree/master/spatialmedia) (для Facebook этого делать не нужно - можно просто закачать видео, и оно будет VR360). 
* Render видео [VR360 в FFMPEG](https://youtu.be/iBGjn4rmFJU?t=756)

