+++
tags = ["davinci", "resolve", "color", "art", "photo"]
description = "Check Exposure in DVR"
title = "Exposure Guide in DVR"
weight = 20
+++

Use Ansel Adams Zone Model to divide exposure in 10 parts 

![](Media02.png)

Check the video by using **Effects -> False Colors**. 
Configure the effect: **Plugin Mode = Creative**, **Creative Style = Exposure Guide**

![](Media03.png)

