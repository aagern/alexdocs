+++
description = "Deliver Room"
title = "Room - Deliver"
weight = 100
+++

### General Web Export

Preset = H.264

Quality restrict to:

-   10000 - 40000 Kb/s - for 1080p
-   20000 - 80000 Kb/s - for 4K UHD/DCI
-   6000 - 9000 Kb/s - for Instagram, 720p. More is not needed since video will be re-encoded by their codec.

For RAW footage:

-   Force sizing to highest quality
-   Force debayer to highest quality