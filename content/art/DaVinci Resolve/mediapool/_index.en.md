+++
tags = ["davinci", "resolve", "video", "audio"]
description = "Create anntations to video"
title = "Annotations to video"
weight = 10
+++

Create anntations to video to future self or other artists:
![](Media01.jpg)

* Use **M,M** to label the created annotation markers;
* Use **ALT+click** on the marker to change its' duration.

Annotations translate to every other room in DaVinci Resolve. 

### Proxy Media
Use Proxy to optimize performance. Use column header Proxy to check if proxies were created for video.

In **Color Room**, turn off proxies to check color drift. 