+++
tags = ["davinci", "resolve", "config", "hotkeys"]
description = "Hotkeys Based on maximum shortcuts for left hand."
title = "DVR Hotkeys"
weight = 30
+++

#### Global Keys

**CTRL+Z** - Undo

**CTRL+SHIFT+Z** - Redo

#### Rooms, Sections

- **SHIFT+1** - Project Manager
- **SHIFT+2** - Media  
- **SHIFT+3** - Cut  
- **SHIFT+4** - Edit  
- **SHIFT+5** - Fusion  
- **SHIFT+6** - Color  
- **SHIFT+7** - Fairlight  
- **SHIFT+8** - Deliver
- **SHIFT+9** - Project Settings

{{% badge %}}EDIT{{% /badge %}} **TAB** - Show/Hide Media Pool

{{% badge %}}EDIT{{% /badge %}} **SHIFT+TAB** - Show/Hide Inspector

**~** - Enter DaVinci Full Screen

**ctrl+~** - See video from Preview in Full Screen (to switch betwwen clips use this from Color tab and **ARROWUP**/**ARROWDOWN**)

#### Playhead keys

- **1** - backwards
- **2** - pause/run
- **3** - forwards
- **4** - In Point
- **5** - Out Point
- **6** - Insert selected to timeline at playhead position

#### In/Out Points

{{% badge %}}EDIT{{% /badge %}} **X** - create In/Out points around selected clip

{{% badge %}}EDIT{{% /badge %}} **ALT+X** - delete In/Out points

{{% badge %}}EDIT{{% /badge %}} **SHIFT+A** - create In/Out points around several selected clips

{{% badge %}}EDIT{{% /badge %}} **Q** - Trim to playhead from left

#### Markers

{{% badge %}}EDIT{{% /badge %}} **M** - Create marker (double-tap **M,M** - create maker & launch marker config window)

{{% badge %}}EDIT{{% /badge %}} **ALT+M** - Delete selected marker 

{{% badge %}}EDIT{{% /badge %}} **SHIFT+ARROWUP/ARROWDOWN** - move playhead between markers

#### Editing Keys

{{% badge %}}EDIT{{% /badge %}} **W** - Split on playhead

{{% badge %}}EDIT{{% /badge %}} **E** - Trim to playhead from right

{{% badge %}}EDIT{{% /badge %}} **S** - Ripple delete selected clip

{{% badge %}}EDIT{{% /badge %}} **D** - Enable/disable clip

{{% badge %}}EDIT{{% /badge %}} **T** - Trim edit mode tool

{{% badge %}}EDIT{{% /badge %}} **CTRL+D** - Delete all gaps between selected clips

{{% badge %}}EDIT{{% /badge %}} - **ARROWUP**/**ARROWDOWN** - Select prev/next clip in timeline

{{% badge %}}EDIT{{% /badge %}} - **ARROWLEFT/ARROWRIGHT** - Move by 1 frame left/right

{{% badge %}}EDIT{{% /badge %}} - **SHIFT+ARROWLEFT/ARROWRIGHT** - Move by 1 second left/right

{{% badge %}}EDIT{{% /badge %}} - **. / ,** - Move selected clip left/right by 1 frame

{{% badge %}}EDIT{{% /badge %}} - **- / = -** Zoom Out/In timeline (Also **ALT+SCROLLBUTTON** or **ALT+SCROLLTRACKPAD** gesture), also **SHIFT+W** for Zoom In 

{{% badge %}}EDIT{{% /badge %}} **CTRL+SCROLLTRACKPAD** - scroll timeline (or **CTRL+SCROLLBUTTON**)

{{% badge %}}EDIT{{% /badge %}} **SHIFT+SCROLLTRACKPAD** - scale in/out track on timeline (or **SHIFT+SCROLLBUTTON**)

{{% badge %}}EDIT{{% /badge %}} - **SHIFT+Z** - Zoom to fit timeline

{{% badge %}}EDIT{{% /badge %}} - **ALT+ARROWUP**/**ARROWDOWN** - bring selected clip up/down a track on timeline

{{% badge %}}EDIT{{% /badge %}} - **SHIFT+~** - Reveal Transform border over selected clip in viewer window

{{% badge %}}EDIT{{% /badge %}} - **CTRL+SHIFT+V** (done after **CTRL+C**) - Paste selected attributes on selected clip / paste clip with no overwrite - moves adjacent clips to the right

{{% badge %}}EDIT{{% /badge %}} - **CTRL+SHIFT+X** - Remove attributes from selected clip 

{{% badge %}}EDIT{{% /badge %}} - **P** - Open clip attributes windows

{{% badge %}}EDIT{{% /badge %}} - **F** - turn snapping ON/OFF (also use **N**)

{{% badge %}}EDIT{{% /badge %}} - **ALT+F** - find selected clip from timeline in Media Pool

{{% badge %}}EDIT{{% /badge %}} - **CTRL+SHIFT+F** - find & show selected clip from Media Pool in Finder

#### Compound Clips

{{% badge %}}EDIT{{% /badge %}} - **CTRL+E** - create a Compound clip from selected clips

{{% badge %}}EDIT{{% /badge %}} - **CTRL+SHIFT+E** - enter inside Compound clip

{{% badge %}}EDIT{{% /badge %}} - **ALT+SHIFT+E** - decompose a Compound clip

#### Clip Timeline Properties / Retime

{{% badge %}}EDIT{{% /badge %}} - **G** - turn linked selection ON/OFF

{{% badge %}}EDIT{{% /badge %}} - **C** - unlink/link video+audio of selected clip

{{% badge %}}EDIT{{% /badge %}} - **R** - Open clip speed window (+Ripple timeline in config will move adjacent clips without overwirting them)

{{% badge %}}EDIT{{% /badge %}} - **SHIFT+R** - clip speed overlay on selected clip

{{% badge %}}EDIT{{% /badge %}} - CTRL+R** - clip speed menu for clip

{{% badge %}}EDIT{{% /badge %}} - **CTRL+SHIFT+R** - show retime curve: **ALT+click** on curve to add speed points fast

{{% badge %}}EDIT{{% /badge %}} - **CTRL+ALT+R** - reset retime controls to 100%

{{% badge %}}EDIT{{% /badge %}} - **SHIFT+C** - Curve Editor on selected clip

{{% badge %}}EDIT{{% /badge %}} - **CTRL+SHIFT+C** - Keyframe Editor on selected clip

{{% badge %}}EDIT{{% /badge %}} - **L** - Loop mode ON/OFF

{{% badge %}}EDIT{{% /badge %}} - **SHIFT+SPACEBAR** - play the loop (use In/Out points to define the loop interval)

**Y** - select clip & press to select all other clips to the right in the track from the current selected one
**CTRL+Y** - clip & press to select all other clips to the left in the track from the current selected one

{{% badge %}}EDIT{{% /badge %}} **H** - show current Timeline you are working in at Media Pool 
