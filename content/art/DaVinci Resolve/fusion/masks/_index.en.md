+++
tags = ["davinci", "resolve", "fusion", "compositing"]
description = "Diagonal masks"
title = "Masks"
weight = 50
+++

### Diagonal Crop

Use **Polygon** mask in Fusion to create a diagonal crop, with underlying video creatively seen.

![](Polygon_crop.jpg)