+++
tags = ["davinci", "resolve", "color", "correction", "management"]
description = "Color management basics"
title = "Color Management"
+++

Videocard sends a signal to the monitor, divided in 2 components: COLORSPACE (color) and GAMMA (light)

### COLORSPACE

Initial colorspace is a triangle-like model of all colors which are discriminated by the human eye (mathematical model):
![](cie1931xy.png)
Different color spaces encompass different sets of colors as compared to this model:
![](latestcolorgamuts.jpg)
rec709 - came with digital TV, P3 - used for digital cinema theater, rec2020 - UHDTV / new 4K video standard.

### GAMMA (Dynamic Range)

Digital cameras see the world in a linear fashion. If we have a room with 1 lamp and we bring in a second lamp, camera will register 2x increase of light. Human eye will register around +20% increase of light.
![](gamma_chart1e.png)
**Transfer function** - math function to compress the light data of an image and then use this file to restore the whole dynamic range seen by the human eye.

-   Human eye dynamic range: 10-13 stops active range, 24 stops total range
-   Film dynamic range: 13 stops
-   RED Weapon Camera: 16.5+ (incl HDRX-mode) stops
-   HLG: 12 stops
-   REC709: 6 stops

REC709 GAMMA 2.4 is the native format for DaVinci Resolve, Adobe Premiere etc. If you throw a different colorspace file in DaVinci, you must tell it what it is and convert: **Color room → Library → ResolveFX Color → Color Space Transform**. 

Fir SONY **SLog3 shoot with +2 or +3 F-stops higher** to quell noise, then choose:

-   Input Colorspace = Sony S-Gamut3
-   Input Gamma = Sony S-Log3
-   Output Colorspace = Rec709
-   Output Gamma = Rec709
-   Tonemapping = Luminance Mapping