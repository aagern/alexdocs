+++
tags = ["davinci", "resolve", "color", "correction"]
description = "Hightlight correction"
title = "Highlights"
+++

### Blown Highlights
They can be restored using several methods:

-   Use the wheels: lower **Gain**, then create node and restore contrast;
-   Use the **Curves**: lower the curve in the highlight area, make "S" form to restore contrast;
-   In the **Curves** instrument, use **Soft Clip** section: **Low / High** restores shadows / highlighs, while **Low Soft / High Soft** makes a smooth transition - ideal for blown sky areas.

### Controlled Restoring of Blown Highlights
* Create a Parallel Node after the input together with the denoise node
![](highlight01.jpg)
* Use Luminance Qualifier to select smoothly the bright part of the image;
* Turn down Highlights to get the details back. 
![](highlight02.jpg)

## Parasite color in highlights
Typically such things appear when shooting on 8bit cameras. Use **Lum Vs Sat Curve** to fix.
![](lumvssat.jpg)

1.  Isolate highlights by luminosity;
2.  Bring saturation level in highlights down to remove color.