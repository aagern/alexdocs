+++
tags = ["davinci", "resolve", "color", "correction", "config"]
description = "Color Room"
title = "Room - Color"
+++

### Articles in section

{{% children sort="weight" description="true" %}}

### Color Room Hotkeys

#### General

-   **SHIFT+D** - Grade Bypass (Disable). Disable all nodes to see the original image;
-   **CTRL+D** - Grade Bypass (Disable) on current node;
-   **SHIFT+H** - see node mask, or qualifier mask etc. in monitor windows;
-   **CTRL+F** - See preview window in Full screen.

#### Create nodes

-   **ALT+S** - New Corrector (Serial) AFTER selected node;
-   **SHIFT+S** - New Corrector (Serial) BEFORE selected node;
-   **ALT+L** - Add Layer lower than current node;
-   **ALT+O** - New Outside node (Includes everything NOT selected in previous node)
-   **ALT+P** - New Parallel node.

#### Tracker

-   **CTRL+T** - Track mask forward;
-   **ALT+T** - Track mask backward.

#### Versions

-   **CTRL+Y** - add new color grade version;
-   **CTRL+N** - switch to next version;
-   **CTRL+B** - switch to previous version.

### PowerGrades

Create a PowerGrade folder to store color grading across all projects.
![](powergrades.jpg)

Captured stills can be put into this folder → go into the DaVinci Database and are shared across all projects. A convenient way to store ready-made Color Space Transforms for different camera colorspace/gamma types. Use **Append Node Graph** to apply the grade, do NOT use **Apply Grade** since this changes the original ISO of the footage,  which may differ in the specific scene.

### Tracker

Cloud tracker - tracks using a whole mask with all its' points.

Point tracker - needs control points on contrast areas, which it uses to track.

### Stills

**Right-click on footage → Grab Still**: this saves the footage with currently applied grade. After any changes to the image, you can find the still in Gallery, and:

-   **double-click** **on still** to make a A|B compare picture. **CTRL+W** to see changes on full screen (disable still);
-   **right-click → Apply Grade** to apply to what was saved. Stills can be applied to any footage to copy correction or try interesting results.

