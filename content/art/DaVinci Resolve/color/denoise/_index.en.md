+++
tags = ["davinci", "resolve", "color", "correction"]
description = "DaVinci Resolve Studion De-Noiser"
title = "De-Noiser"
+++

## Noise

Most of the noise is in red channel. Use DaVinci Resolve Studion version with 2 types of de-noise.
![](noise.jpg)
#### Motion Effects → Temporal NR

-   In case of light noise: **Frames**=3, **Mo. Est. Type**=Faster, **Luma=Chroma**=~10+
-   In case of heavy noise: **Frames**=5, **Mo. Est. Type**=Better, **Luma=Chroma**=~20+

#### Motion Effects → Spatial NR

Unlock **Chroma != Luma**. Raise **Chroma**=6-7

## Moire

Moire on buildings and other landscape objects is countered by motion blur.
![](moire.jpg)
#### Motion Effects → Motion Blur
**Mo. Est. Type**: Better, **Motion Range**=Small, **Motion Blur**=100 (maximum).
