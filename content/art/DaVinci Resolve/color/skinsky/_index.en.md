+++
tags = ["davinci", "resolve", "color", "correction", "grade"]
description = "Skin and Sky exposure grading"
title = "Skin and Sky"
+++

### Skin Exposure

For dark skin (african-american) use contrast+pivot to the left to lighten up, while maintaining contrast:
![](pivot01.jpg)

For pale skin (caucasian) use contrast+pivot to the right to darken, while maintaining contrast:
![](pivot02.jpg)

{{% notice style = "tip" %}}
If you lift the lower midtones, you need to make up for Saturation loss - raise it.
{{% /notice %}}

### Glow
Use **Glow** OpenFX plugin on skin selection to add a bit of skin light (**Shine Threshold** = ~0.6)

### (Quick) Skin Retouch

Skin tones are "Midtones". So use Qualifier and masks to select the skin, and subtract the eyes & brows & mouth.

-   Raise **Color Wheels → (page 2) Midtone Detail (MD)** for boys to underline harshness and manliness;
-   Lower **Color Wheels → (page 2) Midtone Detail (MD)** for girls to smooth out their faces (around -80);
-   Lower overall effect of filter to make it look more natural **Key → Key Output → Gain** = 0.8 - 1.0

### Sky Selection

Use Luminance qualifier for the cleanest sky selection.
![](Sky_qualifier.jpg)
