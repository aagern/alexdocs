+++
tags = ["davinci", "resolve", "color", "correction", "config"]
description = "Color mask types"
title = "Color Masks"
+++

### Using Masks for Correction

Always use the curved mask: it is the only one that has soft transitions, which can be changed individually for different sides:
![](mask_smooth.jpg)

## Neural Masks
Davinci Resolve Studio 18+ brings Object Mask to select parts of a person.
![](objectmask01.jpg)
Draw lines with dropper+ over face & other parts of a person to highlight them. Use dropper- to leave other objects out of the mask. Use **Better** quality with **Refine Edge** BEFORE calculating the mask. 

You can see the mask using Corrector node. 
![](objectmask02.jpg)

Use **Key Mixer node** to combine several masks (you can input more than 2, links are added in **Key Mixer** node automatically upon connection), invert them using button in **Key Mixer** config.
![](objectmask03.jpg)