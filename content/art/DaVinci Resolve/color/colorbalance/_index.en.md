+++
tags = ["davinci", "resolve", "color", "correction"]
description = "Finding the color balance"
title = "Color Balance"
+++

Tip to help reach ideal neutral color balance.
-   Create **Serial Node** for color balance correction  and 2 **Layer nodes**;
-   Apply OpenFX module **Color Generator** to second **Layer Node**;
![](bb01.jpg)
-   In **Color Generator** options choose **Color = HEX #808080** (neutral gray);
-   Choose **Composite Mode = Luminosity**;
![](bb02.jpg)
-   In **balance** **Serial Node**, add +**Saturation** to see color offset;
-   Turn on **Scopes = Parade**;
-   Use **Offset Wheel** to neutralize colors (Parade RGB levels equal);
-   Turn **OFF** the layer node with color generator.