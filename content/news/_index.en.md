+++
archetype = "chapter"
title = "News"
weight = 5
+++

{{% badge %}}01.02.2024{{% /badge %}} 
В процессе решения задачек я потихоньку прихожу к пониманию, что такое "смена мышления" при работе с Rust относительно других языков: это упор на типы данных и конструкции языка заместо привычных общепринятых. Например, использовать перечисления ([enum](dev/rust/enums/index.html)) и match всегда и везде заместо [if-else](dev/rust/flowcontrol/index.html). Попутно я дополнил статью о [векторах](dev/rust/vectors/index.html) темой перевода туда-обратно в строки. Для составных типов данных это наиболее актуальный вопрос: как собрать в такой тип данные, а потом отдать данные назад в правильном формате.

{{% badge %}}28.01.2024{{% /badge %}} 
Этот год я решил вернуться к Rust после перерыва и научиться решать задачки с ним в [codewars](https://www.codewars.com). В процессе я потихоньку осваиваю расширенную библиотеку, например, модуль [itertools](dev/rust/libs/itertools/index.html) тут есть, как и в Python, даёт возможность делать полезные преобразования и сортировку строк.  

{{% badge %}}11.11.2023{{% /badge %}} 
Неожиданное открытие на днях: давно я искал RAW-редактор фото а-ля Lightroom, но при этом бесплатный-открытый, и чтобы при этом умел работать с масками, а вдобавок делать много проб под кривые коррекции в LAB. И такой оказался у меня прямо под носом - Darktable. Я иногда им пользовался, но совершенно проморгал наличие там мощного механизма работы с масками, а вдобавок мощнейших функций шумодава и микроконтраста. Навёрстываю упущенное, создал раздел в заметках и конспект по [шумодаву с масками](art/darktable/index.html)

{{% badge %}}09.11.2023{{% /badge %}} 
Подборка полезных/интересных фреймворков для Rust:
- [Bevy](https://github.com/bevyengine/bevy) - игровой 2D и 3D движок на Rust;
- [Сравнение веб-фреймворков](https://github.com/flosse/rust-web-framework-comparison);
- [Serde](https://serde.rs/) - конвертация между структурами Rust и строками JSON;
- [Tokio](https://tokio.rs/) - асинхронность, распараллеливание потоков данных;
- [Nannou](https://github.com/nannou-org/nannou) - креативное программирование, рисование фрактальных форм;
- [Polars](https://www.pola.rs/) - аналог Pandas, обработка массивов данных;

{{% badge %}}20.10.2023{{% /badge %}} 
Формат "стены" - небольших новостей-блогов - мне показался более интересным для главной страницы сайта. В связи с этим я буду тут писать небольшие заметки, которые далее перемещать в новый [раздел новостей сайта](news/index.html).

Позавчера мы обсудили с коллегами от Kaspersky Labs, VK Cloud и Positive Technologies перспективные технологии на рынке ИБ. Особый упор на ePBF и WebAssembly. Ссылку на стрим я добавил в список [своих видео выступлений](more/credits/index.html) 

{{% badge %}}Более ранние обновления{{% /badge %}} 
[Раздел новостей сайта](news/index.html)

{{% badge %}}06.10.2023{{% /badge %}} 
- Добавил [скалярные переменные Rust](dev/rust/variables/index.html)
- Добавил [популярные строковые методы Rust](dev/rust/strings/index.html)

{{% badge %}}27.09.2023{{% /badge %}} 
- Добавил [про Ingress Resource, nginx rewrite-target](admin/k8s/ingress/index.html)

{{% badge %}}22.09.2023{{% /badge %}} 
- Статья с набором ссылок на [OWASP/CVE исследования](gtd/vuln/index.html)
- Дополнил список [своих видео выступлений](more/credits/index.html)

{{% badge %}}18.09.2023{{% /badge %}} 
- Статья [k8s Ingress](admin/k8s/ingress/index.html)

{{% badge %}}14.09.2023{{% /badge %}} 
- Статья [k8s Taints & Tolerations](admin/k8s/pods/taints/index.html)
- Статья [k8s Node Selectors & Affinity](admin/k8s/pods/affinity/index.html)
- Добавил варианты настройки [Multi-Container Pods](admin/k8s/pods/index.html)
- Статья [k8s Pod Readiness и Liveness Probes](admin/k8s/pods/readiness/index.html)
- Статья по задачам [k8s Jobs](admin/k8s/jobs/index.html)

{{% badge %}}06.09.2023{{% /badge %}} Статья [контексты безопасности в Pod](admin/k8s/pods/securitycontext/index.html)

{{% badge %}}31.08.2023{{% /badge %}} Статья [k8s Pod Commands, Env Variables](admin/k8s/podconfigs/index.html)

{{% badge %}}29.08.2023{{% /badge %}} 
- Обновил статьи по k8s [Deployments](admin/k8s/replicasets/index.html), [Services](admin/k8s/services/index.html), [Pods](admin/k8s/pods/index.html) - добавил ссылки, императивные команды, дополнил описания

{{% badge %}}28.08.2023{{% /badge %}} 
- Обновил статью до [k8s Replica Sets + Deployments](admin/k8s/replicasets/index.html)
- Добавил статью по [k8s Services](admin/k8s/services/index.html)

{{% badge %}}27.08.2023{{% /badge %}} Статья [k8s Replica Sets](admin/k8s/replicasets/index.html)

{{% badge %}}25.08.2023{{% /badge %}} Статья [k8s Pods](admin/k8s/pods/index.html)

{{% badge %}}02.05.2023{{% /badge %}} Раздел [о библиотеках Rust](dev/rust/libs/index.html)

{{% badge %}}27.04.2023{{% /badge %}} [Editing VR360 video in Davinci Resolve](art/davinci-resolve/video360/index.html)

{{% badge %}}25.04.2023{{% /badge %}} 
* Обновление [Rust Hashmap](dev/rust/hashmap/index.html)
* Обновление [Rust Strings](dev/rust/strings/index.html)

{{% badge %}}23.04.2023{{% /badge %}} 
* Обновление [Highlights Correction in DaVinci Resolve](art/davinci-resolve/color/highlights/index.html)

{{% badge %}}05.04.2023{{% /badge %}} 
* [k8s Etcd](admin/k8s/etcd/index.html)
* [k8s kube-apiserver](admin/k8s/kubeapi/index.html)
* [k8s controller Manager](admin/k8s/controller_index.html)

{{% badge %}}04.04.2023{{% /badge %}} [k8s](admin/k8s/index.html)

{{% badge %}}16.03.2023{{% /badge %}} [Rust Traits](dev/rust/traits/index.html)

{{% badge %}}06.03.2023{{% /badge %}} [Rust Generics](dev/rust/generics/index.html)

{{% badge %}}28.02.2023{{% /badge %}} Обновление [Rust Strings](dev/rust/strings/index.html)

{{% badge %}}17.02.2023{{% /badge %}} [Handling errors in Rust](dev/rust/errorhandling/index.html)

{{% badge %}}15.02.2023{{% /badge %}} [Rust Hashmap](dev/rust/hashmap/index.html)

{{% badge %}}13.02.2023{{% /badge %}} 
- [Rust Modules](dev/rust/projectstructure/index.html)
- Обновление [Rust Vectors](dev/rust/vectors/index.html)

{{% badge %}}09.02.2023{{% /badge %}} 
- [Rust Vectors](dev/rust/vectors/index.html)
- [Rust CLI Args](dev/rust/cliargs/index.html)
- [Rust work with Files](dev/rust/files/index.html)

{{% badge %}}08.02.2023{{% /badge %}} 
- Обновление [Rust strings](dev/rust/strings/index.html)
- [Rust Constants](dev/rust/constants/index.html)

{{% badge %}}07.02.2023{{% /badge %}} 
- [Rust Enums](dev/rust/enums/index.html)
- Обновление [Rust strings](dev/rust/strings/index.html)

{{% badge %}}01.02.2023{{% /badge %}} [Rust structs](dev/rust/structures/index.html)

{{% badge %}}30.01.2023{{% /badge %}} 
- [Rust ownership, references](dev/rust/references/index.html)
- [Rust strings](dev/rust/strings/index.html)

{{% badge %}}29.01.2023{{% /badge %}} 
- Статьи по инструментам в [DaVinci Resolve Color Room](art/davinci-resolve/color/index.html)
- [Rust flow control](dev/rust/flowcontrol/index.html)

{{% badge %}}20.01.2023{{% /badge %}} [Rust Variables](dev/rust/variables/index.html)

{{% badge %}}13.01.2023{{% /badge %}}  
- [Mermaid diagrams](gtd/mermaid/index.html)
- [Grafana Loki](admin/loki/index.html)

{{% badge %}}12.01.2023{{% /badge %}}  
+ Статьи по [DaVinci Resolve](art/davinci-resolve/index.html)
+ [Git](dev/git/index.html)
+ Установка [Ansible](admin/ansible/index.html)

{{% badge %}}11.01.2023{{% /badge %}} Начало.