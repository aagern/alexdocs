+++
disableToc = true
title = "Author"
+++

## About the Author

![](Alex_Bletchley.jpg)

### Podcasts
[podcast.digital-work.space](https://podcast.digital-work.space/)
### Talks and Presentations
[White Hacker Interview on MDM and Container technologies](https://vk.com/belyj_haker?z=video-128228754_456239112%2Fvideos-128228754%2Fpl_-128228754_-2)(rus)
An interview on White Hacker channel, Moscow, 20.09.2024

[Offzone Speech on eBPF Process and Network Filtering](https://offzone.moscow/program/filtratsiya-ebpf-v-kubernetes-ili-veselyy-rafting-po-reke-setevykh-dannykh-s-podvodnymi-kamnyami/)(rus)
A speech on Offzone Conference 2024, Moscow, 23.08.2024 [VK.Video](https://vk.com/video-172362100_456239153)

[AM-Live Plus "Sell in 20 minutes section" (rus)](https://www.youtube.com/watch?v=hGn_vLAHHQc)
Presentation of Kaspersky Cloud Workload Security, live show in Moscow, 27.06.2024

[AxSoft DevOps & DevSecOps (rus)](https://m.youtube.com/watch?v=P-3xCNp_zMw&feature=youtu.be)
Webinar presented at AxSoft studio, Moscow, together with Flant,
26.04.2024

[Open Source, AI and other tech trends (rus)](https://www.youtube.com/watch?v=a8tS7P85M28)
Webinar presented at Kaspersky Labs conference room, Moscow city, together with VK Cloud, 18.10.2023

Security on process level (rus): [Kaspersky recording](https://www.youtube.com/watch?v=IMPqX_QcvRc) | [VK Cloud recording](https://www.youtube.com/watch?v=433KVNror6o)
Webinar presented at Kaspersky Labs video room, Moscow city, together with VK Cloud, 03.10.2023

[Security on code level, how the cloud helps in this task (rus)](https://www.youtube.com/live/5T4rlGuWfVg?si=66itLmlx3qVNLlXv)
Webinar presented at Skillbox video room, Moscow city, together with VK Cloud, 22.09.2023

[Introduction to the problematics of Machine Learning (rus)](https://youtu.be/9HeAfHVguzw?si=FkR5slyfaUYkM6Ux)
Webinar presented at Summer Academy, VMware Russia, 24.08.2021

[Discussion of MLOps (rus)](https://www.youtube.com/watch?v=-8cE3Xwh9Xc)
Webinar presented at Summer Academy, VMware Russia, 24.08.2021

[Interview with Brian Madden on VDI topic](https://www.youtube.com/watch?v=lChAs1X7qpw)
Video Interview with Brian, VMware Summer Camp, 24.08.2021

[Interview with Igor Artamonov on ML in Academic Sector(rus)](https://youtu.be/cwE-BGe5j6M?si=cGAiAERu5-2rwH1w)
Video Interview with MAI scientist and teacher, VMware Summer Camp, 24.08.2021

[VDI Monitoring with ControlUP (rus)](https://youtu.be/qKvvhTWz_CM?si=Jr_YGmsmwStFp6bW)
Webinar presented Online, VMware Summer Camp, 24.08.2021

[IT and Business offsite in the COVID period (rus)](https://youtu.be/d3k5OeBbEFk?si=OhM0I1PHAQKgblp4)
Webinar presented Online, VMware Summer Camp, 23.08.2020

[IT and Business offsite - BYOD (rus)](https://youtu.be/dAFtIhwF7Vo?si=y847dmcQtvAEHO4V)
Webinar presented Online, VMware Summer Camp, 09.08.2020

[Kubernetes in VMware (rus)](https://youtu.be/GHN9_X9m58o?si=s0-nOSzE3uw1SMaq)
Speech on DevOps Pro Moscow 2019, 19.11.

[The Platform for University Workflows - pres (rus)](https://icebow.ru/portal/download/attachments/16515136/160512_VUZ_Platform_Ryb.pdf?version=1&modificationDate=1474888951591&api=v2)  
Presented at "[University of the Future](http://universityofthefuture.ru/)" forum, St. Petersburg, 12.05.2016

[Methodical aspects of IT projects made in engineering classes - pres (rus)](https://icebow.ru/portal/download/attachments/16515136/151126_Ryb_Presentation.pdf?version=1&modificationDate=1474886050460&api=v2)  
Presented at School 2015 in Kurkino, Moscow Region, 26.11.2015

[The answers to 10 compliance questions - live (rus)](https://www.youtube.com/watch?v=r9eWyLf4XlU&list=PLSHgmTQvHHVErxN4aSjUYAoE_wefeQ47q&index=2)
Presented at Virtual Russia 2015 in Moscow, 12.11.2015.

[PCoIP Protocol Optimization - live (rus)](https://www.youtube.com/watch?v=N8IJPIyopoQ)  
Presented at Virtual Russia 2014 in Moscow, 05.11.2014.

[Network virtualization with NSX - live (rus)](https://www.youtube.com/watch?v=Z5GcMAtyDnM)  
Presented at Virtual Russia 2014 in Moscow, 16.07.2014.

[VMware vCenter Hyperic Monitorig - webinar (rus)](https://www.youtube.com/watch?v=5qKEXVO6JbI)  
Speech made at VMware Office in Moscow, 24.04.2014.

[Mobile Metrics - pres (site registration needed, rus)](http://2012.russianinternetweek.ru/get/127276/1037/)  
Presented at Russian Internet Week RIW 2012 in Moscow,18.10.2012

### Publications
[DevSecOps RBC Market Analysis (rus)](https://devopsreport.rbc.ru/article-3.html)
Analysis paper conducted together with RBC-NOTA.
31.05.2024

[The High Availability Model of Virtual Container Services in Datacenters (rus)](https://mai.ru/publications/index.php?ID=87356)  
Vestnik MAI Journal, 2017, release 97

[Software Set of Intellectual Support and Security of LMS MAI CLASS.NET (rus, eng)](http://mi.mathnet.ru/eng/vyuru350)  
_Vestnik YuUrGU. Ser. Mat. Model. Progr., 2016, Volume 9, Issue 4_

[Cloud Service Security Modeling Using Virtualization Mechanisms (rus, eng annotation)](http://www.mai.ru/publications/index.php?ID=12514)  
Vestnik MAI Journal, 2009, part 16 N6

[Virtualization - the base of New Generation Computer Security (rus, eng annotation)](http://www.mai.ru/publications/index.php?ID=10224)  
Vestnik MAI Journal, 2009, part 16 N2

### My University Courses 

(Russian language, use [Google Translate](https://translate.google.com/) to figure)

-   [DevOps courses](http://mai.moscow/pages/viewpage.action?pageId=11599878)
-   [Multimedia courses](http://mai.moscow/pages/viewpage.action?pageId=9633840)